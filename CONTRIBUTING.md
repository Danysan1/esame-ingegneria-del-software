# Un'introduzione a git
##### Documentazione: https://git-scm.com/documentation
Git è un sistema (protocollo + client) di versioning (cioè permette di collaborare allo sviluppo di di un progetto software).

E' organizzato in "repository", ovvero "cartelle di codice" e ne gestisce la storia delle modifiche.
Ogni repository può avere più branch, ovvero una copia di se stessa con modifche.
Ogni modifica viene fatta con un "commit".

E' possibile permettere a ogni sviluppatore di eseguire commit sul codice, ma è molto raro.
In genere ogni sviluppatore ha una repository copia della originale (una "fork") su cui effettua le modifiche. 
Quando le modifiche sono pronte, crea una "merge request" con cui chiede di fare confluire i suoi commit nella repository orginale.

GitLab è un server git (ne esistono altri, per esempio https://github.com . Ho scelto questo perchè permette di creare repository private gratis). 

Una funzione molto utile di GitLab è [il servizio di Continuous Integration integrato](https://about.gitlab.com/gitlab-ci/) configurabile con il file .gitlab-ci.yml .
Questo permette di compilare e eseguire i test automaticamente ogni volta che viene fatto un commit o creata una merge request. 


### Per prima cosa creare un fork del progetto:
- Dalla pagina principale della repository cliccare sul pulsante fork in alto


# Come modificare un file dall'interfaccia web
##### Documentazione: https://docs.gitlab.com/ce/user/project/repository/web_editor.html
- Andare nella repository che si vuole modificare (in genere Danysan1/esame... )
- Spostarsi nella cartella e modificare il file
  - se esiste già aprire il file e cliccare `Edit` (nota: l'interfaccia web non supporta i file binari come pdf, file office o immagini)
  - se deve essere creato cliccare il `+` e selezionare `New file`
  - se deve essere caricato cliccare il `+` e selezionare `Upload file`
- Cliccare `Commit changes`

Se si è nella propria repository la modifica verrà applicata immediatamente, altrimenti verrà creato un branch nella pripria repository e un merge request per la destinazione.


# Guida per modificare i file markdown (.md)
- https://guides.github.com/features/mastering-markdown/
- https://gitlab.com/help/user/markdown
- http://daringfireball.net/projects/markdown/

# Come usare git da Visual Studio
##### Documentazione: https://www.visualstudio.com/en-us/docs/git/gitquickstart
- Scaricare il progetto (va fatto solo una volta)
  - Dalla finestra `Team Explorer`, sotto `Local Git Repositories`, clicare su `Clone`
  - Inserire nserire il link della propria repository: `https://gitlab.com/$UTENTE/esame-ingegneria-del-software` 
    (Dove $UTENTE è il nome utente su GitLab)
    (Chiederà nome utente e password di GitLab)
- Aprire la soluzione di Visual Studio (se non la apre automaticamente)
  - Dalla finestra `Team Explorer`, nella sezione `Solutions`, fare doppio click sulla soluzione che si vuole aprire
- Scaricare modifiche fatte e caricate (`git push`) da altri computer o dall'interfaccia web
  - Dalla finestra `Team Explorer` cliccare `Sync`
  - Sotto `Incoming Commits` cliccare su `Fetch` e `Pull`
- Importare nella propria repository le modifiche fatte da qualcun'altro ([fonte](https://writeabout.net/2016/01/31/sync-your-git-github-fork-upstream-repository-from-within-visual-studio/))
  - L'altro utente deve essere già stato aggiunto come riferimenti remoto (vedi ultimo punto)
  - Dalla finestra `Team Explorer` cliccare `Sync`
  - Sotto `Incoming Commits` cliccare sulla freccia a fianco di `Fetch` e selezionare l'utente di cui scaricare il codice
  - Tornare alla home del `Team Explorer` e cliccare `Branches`
  - Cliccare con il destro sul branch da scaricare e selezionare `Merge from...`
  - Cliccare `Merge`
- Caricare una modifica:
  - Dalla finestra `Team Explorer` cliccare `Changes`
  - Inserire titolo e descrizione delle modifiche fatte nella finestrella gialla
  - Se si vuole solo fare il commit cliccare `Commit All`
    (in seguito, per caricare i commit, dalla alla finestra `Team Explorer` cliccare `Sync` > `Push`)
  - Se si vuole fare il commit e caricarlo cliccare su `Commit All and Push`
- Aggiungere gli altri utenti come riferimenti remoti (va fatto solo una volta)
  - Dalla finestra `Team Explorer` cliccare su `Settings` > `Repository Settings`
  - Sotto `Remotes` cliccare su `Add`
  - Inserire in Name il nome dell'altra persona
  - Inserire in Fetch `https://gitlab.com/$UTENTE/esame-ingegneria-del-software`
    (Dove $UTENTE è il nome utente su GitLab dell'altra persona)


# Come usare git da linea di comando
##### Documentazione: https://git-scm.com/docs
##### Documentazione su linea di comando (solo su linux): `man git` ([versione web](https://linux.die.net/man/1/git))
##### Cheatsheet: https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf
- Per scaricare il progetto:
  - `git clone https://gitlab.com/$UTENTE/esame-ingegneria-del-software`
    (Dove $UTENTE è il nome utente su GitLab)
- Per caricare una modifica:
  - `git add $FILES`
    (Dove $FILES è la lista dei file modificati che vanno caricati)
  - `git commit`
    (Chiederà titolo e descrizione delle modifiche fatte)
  - `git push`
    (Chiederà utente e password di GitLab)
    (Se la repository è in conflitto con quella remota è possibile usare `git push --force`, **ma va usata con cautela, perchè sovrascrive completamente la repository remota!** [:S](http://i.imgur.com/2A3ihEW.gif)).
- Per aggiungere gli altri utenti come riferimenti remoti 
  - `git remote add $NOME https://gitlab.com/$UTENTE/esame-ingegneria-del-software`
    (Dove $NOME è il nome dell'altra persona e $UTENTE è il nome utente su GitLab dell'altra persona)
- Per importare nella propria repository le modifiche fatte da qualcun'altro
  - `git fetch --all`
  - `git merge $NOME/$BRANCH`
    (Dove $NOME è il nome della persona e $BRANCH è il branch da importare, per noi in genere sarà `master`)
- Per scaricare modifiche fatte e caricate (`git push`) da altri computer
  - `git fetch`
  - `git pull`
- Per resettare la repository locale (sovrascriverla con il contenuto di una repository remota)
  - `git reset --hard $REPO/$BRANCH`
    (Dove $REPO è la repository e $BRANCH il branch il cui contenuto verrà usato. In genere sarà `git pull origin/master`)


# Tipico workflow
##### Approfondimento: https://www.atlassian.com/git/tutorials/comparing-workflows
- Controlla le modifiche fatte alla repository (`git fetch`)
- Scarica le modifiche fatte sulla propria repository con altri computer o dall'interfaccia web (`git pull`)
- Scarica le modifiche fatte sulla repository principale (`git merge`)
- Fai le modifiche che vuoi fare
- Controlla le modifiche fatte (`git diff`)
- Conferma le modifiche fatte (`git add`)
- Prepara le modifiche per l'invio (`git commit`)
- Invia le modifiche alla propria repository (`git push`)
- Chiedi al gestore della repository principale di aggiungere le tue modifiche alla repository principale (merge request)


# Come creare una merge request dall'interfaccia web di GitLab
- Dalla pagina principale della propria repository cliccare `Merge requests`
- Cliccare `New Merge Request`
- Controllare che il `Source Branch` sia il branch con le proprie modifiche (in genere `master` dalla propria repository)
- Controllare che il `Target Branch` sia `master` da `Danysan1/esame-ingegneria-del-software`
- Cliccare `Compare branches and continue`
- Modificare titolo e descrizione
- Cliccare `Submit merge request`


# Come permettere agli altri di vedere la propria repository
- Andare in `https://gitlab.com/$UTENTE/esame-ingegneria-del-software/settings/members` (Dove $UTENTE è il proprio nome utente)
- Inserire nel primo campo i nomi utenti degli utenti da invitare
- Nel secondo campo selezionare `Reporter` (dettagli dei permessi: https://gitlab.com/help/user/permissions )
- Cliccare `Add to project`

# Come modificare la password git ricordata da Visual Studio
##### Fonte: https://blogs.msdn.microsoft.com/visualstudioalm/2012/08/29/clearing-the-credentials-for-connecting-to-a-team-foundation-server/
Visual Studio memorizza la passord di GitLab automaticamente al primo utilizzo per evitare di chiederla nuovamente. Se si cambia in seguito la password di GitLab, Visual Studio ricorderà la password sbagliata e segnalerà un errore durante la sincronizzazione. Per risolverlo andare in `Pannello di controllo` > `Account utente` > `Gestione credenziali Windows`, selezionare GitLab e cliccare `modifica`.