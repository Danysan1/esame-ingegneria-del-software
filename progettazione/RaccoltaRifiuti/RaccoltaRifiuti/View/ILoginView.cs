﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View
{
    interface ILoginView
    {
        event FormClosedEventHandler FormClosed;
        TextBox CodiceFiscale { get; }
        TextBox Password { get; }
        Button Login { get; }
        void Show();
        void Hide();
    }
}
