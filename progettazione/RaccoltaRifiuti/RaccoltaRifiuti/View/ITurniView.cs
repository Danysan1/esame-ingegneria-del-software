﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View
{
    public interface ITurniView
    {
        ComboBox Percorso { get; }
        ComboBox Camion { get; }
        ComboBox Tipo { get; }
        CheckedListBox Equipaggio { get; }
        Button Aggiungi { get; }
        Button Rimuovi { get; }
        DataGridView GridTurno { get; }
        Panel PanelTurno { get; }
    }
}
