﻿namespace RaccoltaRifiuti.View.WindowsForms
{
    partial class GestionePercorsiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this._durataPrevista = new System.Windows.Forms.DateTimePicker();
            this._rimuovi = new System.Windows.Forms.Button();
            this._aggiungi = new System.Windows.Forms.Button();
            this._checkedListBox = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this._comboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this._dataGridView = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this._durataPrevista);
            this.panel1.Controls.Add(this._rimuovi);
            this.panel1.Controls.Add(this._aggiungi);
            this.panel1.Controls.Add(this._checkedListBox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this._comboBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(896, 56);
            this.panel1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Durata Prevista:";
            // 
            // _durataPrevista
            // 
            this._durataPrevista.CustomFormat = "HH:mm";
            this._durataPrevista.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._durataPrevista.Location = new System.Drawing.Point(92, 33);
            this._durataPrevista.Name = "_durataPrevista";
            this._durataPrevista.ShowUpDown = true;
            this._durataPrevista.Size = new System.Drawing.Size(131, 20);
            this._durataPrevista.TabIndex = 6;
            this._durataPrevista.Value = new System.DateTime(2001, 1, 1, 0, 0, 0, 0);
            // 
            // _rimuovi
            // 
            this._rimuovi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._rimuovi.Enabled = false;
            this._rimuovi.Location = new System.Drawing.Point(809, 6);
            this._rimuovi.Name = "_rimuovi";
            this._rimuovi.Size = new System.Drawing.Size(75, 23);
            this._rimuovi.TabIndex = 5;
            this._rimuovi.Text = "Rimuovi";
            this._rimuovi.UseVisualStyleBackColor = true;
            // 
            // _aggiungi
            // 
            this._aggiungi.Enabled = false;
            this._aggiungi.Location = new System.Drawing.Point(613, 6);
            this._aggiungi.Name = "_aggiungi";
            this._aggiungi.Size = new System.Drawing.Size(75, 23);
            this._aggiungi.TabIndex = 4;
            this._aggiungi.Text = "Aggiungi";
            this._aggiungi.UseVisualStyleBackColor = true;
            // 
            // _checkedListBox
            // 
            this._checkedListBox.FormattingEnabled = true;
            this._checkedListBox.Location = new System.Drawing.Point(433, 3);
            this._checkedListBox.Name = "_checkedListBox";
            this._checkedListBox.Size = new System.Drawing.Size(174, 49);
            this._checkedListBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(341, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Punti di raccolta:";
            // 
            // _comboBox
            // 
            this._comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBox.FormattingEnabled = true;
            this._comboBox.Location = new System.Drawing.Point(71, 6);
            this._comboBox.Name = "_comboBox";
            this._comboBox.Size = new System.Drawing.Size(264, 21);
            this._comboBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Destinazione:";
            // 
            // _dataGridView
            // 
            this._dataGridView.AllowUserToOrderColumns = true;
            this._dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._dataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this._dataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dataGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this._dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataGridView.Location = new System.Drawing.Point(0, 56);
            this._dataGridView.Name = "_dataGridView";
            this._dataGridView.RowHeadersVisible = false;
            this._dataGridView.Size = new System.Drawing.Size(896, 282);
            this._dataGridView.TabIndex = 1;
            // 
            // GestionePercorsiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 338);
            this.Controls.Add(this._dataGridView);
            this.Controls.Add(this.panel1);
            this.Name = "GestionePercorsiForm";
            this.Text = "Gestione Percorsi";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView _dataGridView;
        private System.Windows.Forms.CheckedListBox _checkedListBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox _comboBox;
        private System.Windows.Forms.Button _rimuovi;
        private System.Windows.Forms.Button _aggiungi;
        private System.Windows.Forms.DateTimePicker _durataPrevista;
        private System.Windows.Forms.Label label3;
    }
}