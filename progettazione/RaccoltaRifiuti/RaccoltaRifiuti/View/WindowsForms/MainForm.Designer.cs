﻿namespace RaccoltaRifiuti.View.WindowsForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._carica = new System.Windows.Forms.ToolStripMenuItem();
            this._logout = new System.Windows.Forms.ToolStripMenuItem();
            this._esci = new System.Windows.Forms.ToolStripMenuItem();
            this._gestione = new System.Windows.Forms.ToolStripMenuItem();
            this._salva = new System.Windows.Forms.ToolStripMenuItem();
            this._salvaSu = new System.Windows.Forms.ToolStripMenuItem();
            this._camion = new System.Windows.Forms.ToolStripMenuItem();
            this._destinzioni = new System.Windows.Forms.ToolStripMenuItem();
            this._persone = new System.Windows.Forms.ToolStripMenuItem();
            this._credenziali = new System.Windows.Forms.ToolStripMenuItem();
            this._puntiDiRaccolta = new System.Windows.Forms.ToolStripMenuItem();
            this._tipiDiRifiuto = new System.Windows.Forms.ToolStripMenuItem();
            this._Percorsi = new System.Windows.Forms.ToolStripMenuItem();
            this._calendario = new System.Windows.Forms.DataGridView();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._calendario)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this._gestione});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(894, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._carica,
            this._logout,
            this._esci});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // _carica
            // 
            this._carica.Name = "_carica";
            this._carica.Size = new System.Drawing.Size(132, 22);
            this._carica.Text = "Carica da...";
            // 
            // _logout
            // 
            this._logout.Name = "_logout";
            this._logout.Size = new System.Drawing.Size(132, 22);
            this._logout.Text = "Logout";
            // 
            // _esci
            // 
            this._esci.Name = "_esci";
            this._esci.Size = new System.Drawing.Size(132, 22);
            this._esci.Text = "Esci";
            // 
            // _gestione
            // 
            this._gestione.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._salva,
            this._salvaSu,
            this._camion,
            this._destinzioni,
            this._persone,
            this._credenziali,
            this._puntiDiRaccolta,
            this._tipiDiRifiuto,
            this._Percorsi});
            this._gestione.Name = "_gestione";
            this._gestione.Size = new System.Drawing.Size(65, 20);
            this._gestione.Text = "Gestione";
            // 
            // _salva
            // 
            this._salva.Name = "_salva";
            this._salva.Size = new System.Drawing.Size(163, 22);
            this._salva.Text = "Salva";
            // 
            // _salvaSu
            // 
            this._salvaSu.Name = "_salvaSu";
            this._salvaSu.Size = new System.Drawing.Size(163, 22);
            this._salvaSu.Text = "Salva su...";
            // 
            // _camion
            // 
            this._camion.Name = "_camion";
            this._camion.Size = new System.Drawing.Size(163, 22);
            this._camion.Text = "Camion";
            // 
            // _destinzioni
            // 
            this._destinzioni.Name = "_destinzioni";
            this._destinzioni.Size = new System.Drawing.Size(163, 22);
            this._destinzioni.Text = "Destinazioni";
            // 
            // _persone
            // 
            this._persone.Name = "_persone";
            this._persone.Size = new System.Drawing.Size(163, 22);
            this._persone.Text = "Persone";
            // 
            // _credenziali
            // 
            this._credenziali.Name = "_credenziali";
            this._credenziali.Size = new System.Drawing.Size(163, 22);
            this._credenziali.Text = "Credenziali";
            // 
            // _puntiDiRaccolta
            // 
            this._puntiDiRaccolta.Name = "_puntiDiRaccolta";
            this._puntiDiRaccolta.Size = new System.Drawing.Size(163, 22);
            this._puntiDiRaccolta.Text = "Punti di Raccolta";
            // 
            // _tipiDiRifiuto
            // 
            this._tipiDiRifiuto.Name = "_tipiDiRifiuto";
            this._tipiDiRifiuto.Size = new System.Drawing.Size(163, 22);
            this._tipiDiRifiuto.Text = "Tipi di Rifiuto";
            // 
            // _Percorsi
            // 
            this._Percorsi.Name = "_Percorsi";
            this._Percorsi.Size = new System.Drawing.Size(163, 22);
            this._Percorsi.Text = "Percorsi";
            // 
            // _calendario
            // 
            this._calendario.AllowUserToOrderColumns = true;
            this._calendario.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._calendario.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this._calendario.BackgroundColor = System.Drawing.SystemColors.Control;
            this._calendario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._calendario.DefaultCellStyle = dataGridViewCellStyle1;
            this._calendario.Dock = System.Windows.Forms.DockStyle.Fill;
            this._calendario.Location = new System.Drawing.Point(0, 24);
            this._calendario.Name = "_calendario";
            this._calendario.ReadOnly = true;
            this._calendario.RowHeadersVisible = false;
            this._calendario.Size = new System.Drawing.Size(894, 390);
            this._calendario.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 414);
            this.Controls.Add(this._calendario);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Raccolta Rifiuti";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._calendario)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _carica;
        private System.Windows.Forms.ToolStripMenuItem _logout;
        private System.Windows.Forms.ToolStripMenuItem _esci;
        private System.Windows.Forms.ToolStripMenuItem _gestione;
        private System.Windows.Forms.ToolStripMenuItem _camion;
        private System.Windows.Forms.ToolStripMenuItem _destinzioni;
        private System.Windows.Forms.ToolStripMenuItem _persone;
        private System.Windows.Forms.ToolStripMenuItem _puntiDiRaccolta;
        private System.Windows.Forms.ToolStripMenuItem _tipiDiRifiuto;
        private System.Windows.Forms.ToolStripMenuItem _Percorsi;
        private System.Windows.Forms.ToolStripMenuItem _salva;
        private System.Windows.Forms.ToolStripMenuItem _salvaSu;
        private System.Windows.Forms.DataGridView _calendario;
        private System.Windows.Forms.ToolStripMenuItem _credenziali;
    }
}