﻿namespace RaccoltaRifiuti.View.WindowsForms
{
    partial class TurnoGiornalieroControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._oraInizio = new System.Windows.Forms.DateTimePicker();
            this._dataFine = new System.Windows.Forms.DateTimePicker();
            this._dataInizio = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data di Inizio:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Data di Fine:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ora di Inizio:";
            // 
            // _oraInizio
            // 
            this._oraInizio.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this._oraInizio.Location = new System.Drawing.Point(113, 64);
            this._oraInizio.Name = "_oraInizio";
            this._oraInizio.ShowUpDown = true;
            this._oraInizio.Size = new System.Drawing.Size(109, 20);
            this._oraInizio.TabIndex = 5;
            this._oraInizio.Value = new System.DateTime(2001, 1, 1, 0, 0, 0, 0);
            // 
            // _dataFine
            // 
            this._dataFine.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this._dataFine.Location = new System.Drawing.Point(113, 32);
            this._dataFine.Name = "_dataFine";
            this._dataFine.Size = new System.Drawing.Size(109, 20);
            this._dataFine.TabIndex = 6;
            this._dataFine.Value = new System.DateTime(2017, 6, 8, 0, 0, 0, 0);
            // 
            // _dataInizio
            // 
            this._dataInizio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this._dataInizio.Location = new System.Drawing.Point(113, 3);
            this._dataInizio.Name = "_dataInizio";
            this._dataInizio.Size = new System.Drawing.Size(109, 20);
            this._dataInizio.TabIndex = 7;
            this._dataInizio.Value = new System.DateTime(2017, 6, 8, 0, 0, 0, 0);
            // 
            // TurnoGiornalieroControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._dataInizio);
            this.Controls.Add(this._dataFine);
            this.Controls.Add(this._oraInizio);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "TurnoGiornalieroControl";
            this.Size = new System.Drawing.Size(225, 100);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker _oraInizio;
        private System.Windows.Forms.DateTimePicker _dataFine;
        private System.Windows.Forms.DateTimePicker _dataInizio;
    }
}
