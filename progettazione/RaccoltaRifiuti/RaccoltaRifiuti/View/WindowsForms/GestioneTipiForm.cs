﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View.WindowsForms
{
    public partial class GestioneTipiForm : Form, ITipiDiRifiutoView
    {
        public DataGridView TipiView { get { return _tipiRifiutiDataGridView; } }
        public TextBox Nome { get { return _nome; } }
        public Button Aggiungi { get { return _aggiungi; } }
        public Button Rimuovi { get { return _rimuovi; } }

        public GestioneTipiForm()
        {
            InitializeComponent();
        }

    }
}
