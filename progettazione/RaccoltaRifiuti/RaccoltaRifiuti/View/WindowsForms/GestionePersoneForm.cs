﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View.WindowsForms
{
    public partial class GestionePersoneForm : Form, IPersoneView
    {
        public Button AggiungiUtente { get { return _aggiungiUtenteButton; } }
        public Button RimuoviUtente { get { return _rimuoviUtenteButton; } }
        public TextBox NomeUtente { get { return _nomeUtenteBox; } }
        public TextBox CognomeUtente { get { return _cognomeUtenteBox; } }
        public TextBox CodiceFiscaleUtente { get { return _cfUtenteBox; } }
        public DataGridView Utenti { get { return _utentiView; } }

        public GestionePersoneForm()
        {
            InitializeComponent();
        }
    }
}
