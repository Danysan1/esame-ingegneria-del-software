﻿namespace RaccoltaRifiuti.View.WindowsForms
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._codice = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._password = new System.Windows.Forms.TextBox();
            this._login = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _codice
            // 
            this._codice.Location = new System.Drawing.Point(94, 15);
            this._codice.Name = "_codice";
            this._codice.Size = new System.Drawing.Size(172, 20);
            this._codice.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Codice Fiscale";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password";
            // 
            // _password
            // 
            this._password.AcceptsReturn = true;
            this._password.Location = new System.Drawing.Point(94, 41);
            this._password.Name = "_password";
            this._password.PasswordChar = '*';
            this._password.Size = new System.Drawing.Size(172, 20);
            this._password.TabIndex = 2;
            // 
            // _login
            // 
            this._login.Enabled = false;
            this._login.Location = new System.Drawing.Point(94, 67);
            this._login.Name = "_login";
            this._login.Size = new System.Drawing.Size(75, 23);
            this._login.TabIndex = 4;
            this._login.Text = "Login";
            this._login.UseVisualStyleBackColor = true;
            // 
            // LoginForm
            // 
            this.AcceptButton = this._login;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 119);
            this.Controls.Add(this._login);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._password);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._codice);
            this.Name = "LoginForm";
            this.Text = "Raccolta Rifiuti";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _codice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _password;
        private System.Windows.Forms.Button _login;
    }
}