﻿namespace RaccoltaRifiuti.View.WindowsForms
{
    partial class GestioneIndirizzoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this._rimuovi = new System.Windows.Forms.Button();
            this._aggiungi = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this._indirizzo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._nome = new System.Windows.Forms.TextBox();
            this._destinazioniView = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._destinazioniView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this._rimuovi);
            this.panel1.Controls.Add(this._aggiungi);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this._indirizzo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this._nome);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(900, 30);
            this.panel1.TabIndex = 0;
            // 
            // _rimuovi
            // 
            this._rimuovi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._rimuovi.Enabled = false;
            this._rimuovi.Location = new System.Drawing.Point(822, 4);
            this._rimuovi.Name = "_rimuovi";
            this._rimuovi.Size = new System.Drawing.Size(75, 23);
            this._rimuovi.TabIndex = 5;
            this._rimuovi.Text = "Rimuovi";
            this._rimuovi.UseVisualStyleBackColor = true;
            // 
            // _aggiungi
            // 
            this._aggiungi.Enabled = false;
            this._aggiungi.Location = new System.Drawing.Point(424, 4);
            this._aggiungi.Name = "_aggiungi";
            this._aggiungi.Size = new System.Drawing.Size(75, 23);
            this._aggiungi.TabIndex = 4;
            this._aggiungi.Text = "Aggiungi";
            this._aggiungi.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(201, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Indirizzo:";
            // 
            // _indirizzo
            // 
            this._indirizzo.Location = new System.Drawing.Point(255, 6);
            this._indirizzo.Name = "_indirizzo";
            this._indirizzo.Size = new System.Drawing.Size(163, 20);
            this._indirizzo.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome:";
            // 
            // _nome
            // 
            this._nome.Location = new System.Drawing.Point(47, 6);
            this._nome.Name = "_nome";
            this._nome.Size = new System.Drawing.Size(148, 20);
            this._nome.TabIndex = 0;
            // 
            // _destinazioniView
            // 
            this._destinazioniView.AllowUserToOrderColumns = true;
            this._destinazioniView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._destinazioniView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._destinazioniView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._destinazioniView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._destinazioniView.Location = new System.Drawing.Point(0, 30);
            this._destinazioniView.Name = "_destinazioniView";
            this._destinazioniView.RowHeadersVisible = false;
            this._destinazioniView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._destinazioniView.Size = new System.Drawing.Size(900, 380);
            this._destinazioniView.TabIndex = 1;
            // 
            // GestioneIndirizzoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 410);
            this.Controls.Add(this._destinazioniView);
            this.Controls.Add(this.panel1);
            this.Name = "GestioneIndirizzoForm";
            this.Text = "Gestione Destinazioni";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._destinazioniView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox _nome;
        private System.Windows.Forms.DataGridView _destinazioniView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _indirizzo;
        private System.Windows.Forms.Button _aggiungi;
        private System.Windows.Forms.Button _rimuovi;
    }
}