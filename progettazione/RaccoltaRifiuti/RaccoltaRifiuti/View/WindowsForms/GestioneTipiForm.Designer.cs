﻿namespace RaccoltaRifiuti.View.WindowsForms
{
    partial class GestioneTipiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._tipiRifiutiDataGridView = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this._rimuovi = new System.Windows.Forms.Button();
            this._nome = new System.Windows.Forms.TextBox();
            this._aggiungi = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._tipiRifiutiDataGridView)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tipiRifiutiDataGridView
            // 
            this._tipiRifiutiDataGridView.AllowUserToOrderColumns = true;
            this._tipiRifiutiDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._tipiRifiutiDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this._tipiRifiutiDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tipiRifiutiDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tipiRifiutiDataGridView.Location = new System.Drawing.Point(0, 30);
            this._tipiRifiutiDataGridView.Name = "_tipiRifiutiDataGridView";
            this._tipiRifiutiDataGridView.RowHeadersVisible = false;
            this._tipiRifiutiDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._tipiRifiutiDataGridView.Size = new System.Drawing.Size(898, 384);
            this._tipiRifiutiDataGridView.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this._rimuovi);
            this.panel1.Controls.Add(this._nome);
            this.panel1.Controls.Add(this._aggiungi);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(898, 30);
            this.panel1.TabIndex = 1;
            // 
            // _rimuovi
            // 
            this._rimuovi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._rimuovi.Enabled = false;
            this._rimuovi.Location = new System.Drawing.Point(820, 4);
            this._rimuovi.Name = "_rimuovi";
            this._rimuovi.Size = new System.Drawing.Size(75, 23);
            this._rimuovi.TabIndex = 3;
            this._rimuovi.Text = "Rimuovi";
            this._rimuovi.UseVisualStyleBackColor = true;
            // 
            // _nome
            // 
            this._nome.Location = new System.Drawing.Point(47, 6);
            this._nome.Name = "_nome";
            this._nome.Size = new System.Drawing.Size(100, 20);
            this._nome.TabIndex = 2;
            // 
            // _aggiungi
            // 
            this._aggiungi.Enabled = false;
            this._aggiungi.Location = new System.Drawing.Point(153, 4);
            this._aggiungi.Name = "_aggiungi";
            this._aggiungi.Size = new System.Drawing.Size(75, 23);
            this._aggiungi.TabIndex = 1;
            this._aggiungi.Text = "Aggiungi";
            this._aggiungi.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome:";
            // 
            // GestioneTipiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 414);
            this.Controls.Add(this._tipiRifiutiDataGridView);
            this.Controls.Add(this.panel1);
            this.Name = "GestioneTipiForm";
            this.Text = "Gestione Tipi";
            ((System.ComponentModel.ISupportInitialize)(this._tipiRifiutiDataGridView)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView _tipiRifiutiDataGridView;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button _rimuovi;
        private System.Windows.Forms.TextBox _nome;
        private System.Windows.Forms.Button _aggiungi;
        private System.Windows.Forms.Label label1;
    }
}