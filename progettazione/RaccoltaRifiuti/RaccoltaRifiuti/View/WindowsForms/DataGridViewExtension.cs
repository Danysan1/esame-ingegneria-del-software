﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View.WindowsForms
{
    public static class DataGridViewExtension
    {
        public static void MostraColonna(this DataGridView grid, string nome, bool visible)
        {
            foreach (DataGridViewColumn c in grid.Columns)
                if (c.DataPropertyName == nome)
                    c.Visible = visible;
        }

        public static void AllargaColonna(this DataGridView grid, string nome)
        {
            if (nome != null)
            {
                foreach (DataGridViewColumn c in grid.Columns)
                {
                    if (c.DataPropertyName == nome)
                    {
                        c.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                        return;
                    }
                }
            }

            if(grid.Columns.Count > 0)
                grid.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        public static IEnumerable OggettiSelezionati(this DataGridView grid)
        {
            return from DataGridViewRow riga in grid.SelectedRows select riga.DataBoundItem;
        }
    }
}
