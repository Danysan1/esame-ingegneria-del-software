﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View.WindowsForms
{
    public partial class GestioneIndirizzoForm : Form, IDestinazioniView, IPuntiDiRaccoltaView
    {
        public Button Aggiungi { get { return _aggiungi; } }
        public Button Rimuovi { get { return _rimuovi; } }
        public TextBox Nome { get { return _nome; } }
        public TextBox Indirizzo { get { return _indirizzo; } }
        public DataGridView DestinazioniView { get { return _destinazioniView; } }
        public DataGridView PuntiDiRaccoltaView { get { return _destinazioniView; } }

        public GestioneIndirizzoForm()
        {
            InitializeComponent();
        }
    }
}
