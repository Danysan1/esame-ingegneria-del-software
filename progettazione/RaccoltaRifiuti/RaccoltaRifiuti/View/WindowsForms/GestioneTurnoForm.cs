﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View.WindowsForms
{
    public partial class GestioneTurnoForm : Form, ITurniView
    {

        public ComboBox Percorso { get { return _Percorso; } }
        public ComboBox Camion { get { return _camion; } }
        public ComboBox Tipo { get { return _tipo; } }
        public CheckedListBox Equipaggio { get { return _equipaggioBox; } }
        public Button Aggiungi { get { return _aggiungi; } }
        public Button Rimuovi { get {return _rimuovi;}}
        public DataGridView GridTurno {get{return _TurniGrid;}}
        public Panel PanelTurno { get { return _tipoTurnoPanel; } }
        public GestioneTurnoForm()
        {
            InitializeComponent();
        }
    }
}
