﻿namespace RaccoltaRifiuti.View.WindowsForms
{
    partial class GestioneTurnoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this._TurniGrid = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this._Percorso = new System.Windows.Forms.ComboBox();
            this._aggiungi = new System.Windows.Forms.Button();
            this._rimuovi = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this._tipoTurnoPanel = new System.Windows.Forms.Panel();
            this._tipo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._equipaggioBox = new System.Windows.Forms.CheckedListBox();
            this._camion = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this._TurniGrid)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _TurniGrid
            // 
            this._TurniGrid.AllowUserToOrderColumns = true;
            this._TurniGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._TurniGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this._TurniGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._TurniGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._TurniGrid.DefaultCellStyle = dataGridViewCellStyle1;
            this._TurniGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._TurniGrid.Location = new System.Drawing.Point(0, 111);
            this._TurniGrid.Name = "_TurniGrid";
            this._TurniGrid.RowHeadersVisible = false;
            this._TurniGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._TurniGrid.Size = new System.Drawing.Size(905, 316);
            this._TurniGrid.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Percorso:";
            // 
            // _Percorso
            // 
            this._Percorso.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Percorso.FormattingEnabled = true;
            this._Percorso.Location = new System.Drawing.Point(93, 6);
            this._Percorso.Name = "_Percorso";
            this._Percorso.Size = new System.Drawing.Size(121, 21);
            this._Percorso.TabIndex = 1;
            // 
            // _aggiungi
            // 
            this._aggiungi.Enabled = false;
            this._aggiungi.Location = new System.Drawing.Point(643, 42);
            this._aggiungi.Name = "_aggiungi";
            this._aggiungi.Size = new System.Drawing.Size(75, 23);
            this._aggiungi.TabIndex = 2;
            this._aggiungi.Text = "Aggiungi";
            this._aggiungi.UseVisualStyleBackColor = true;
            // 
            // _rimuovi
            // 
            this._rimuovi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._rimuovi.Enabled = false;
            this._rimuovi.Location = new System.Drawing.Point(818, 42);
            this._rimuovi.Name = "_rimuovi";
            this._rimuovi.Size = new System.Drawing.Size(75, 23);
            this._rimuovi.TabIndex = 3;
            this._rimuovi.Text = "Rimuovi";
            this._rimuovi.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Camion:";
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this._tipoTurnoPanel);
            this.panel1.Controls.Add(this._tipo);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this._equipaggioBox);
            this.panel1.Controls.Add(this._camion);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this._rimuovi);
            this.panel1.Controls.Add(this._aggiungi);
            this.panel1.Controls.Add(this._Percorso);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(905, 111);
            this.panel1.TabIndex = 0;
            // 
            // _tipoTurnoPanel
            // 
            this._tipoTurnoPanel.Location = new System.Drawing.Point(412, 5);
            this._tipoTurnoPanel.Name = "_tipoTurnoPanel";
            this._tipoTurnoPanel.Size = new System.Drawing.Size(225, 100);
            this._tipoTurnoPanel.TabIndex = 10;
            // 
            // _tipo
            // 
            this._tipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._tipo.FormattingEnabled = true;
            this._tipo.Location = new System.Drawing.Point(93, 87);
            this._tipo.Name = "_tipo";
            this._tipo.Size = new System.Drawing.Size(121, 21);
            this._tipo.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Tipo di Rifiuto:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(232, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Equipaggio:";
            // 
            // _equipaggioBox
            // 
            this._equipaggioBox.CheckOnClick = true;
            this._equipaggioBox.FormattingEnabled = true;
            this._equipaggioBox.Location = new System.Drawing.Point(220, 29);
            this._equipaggioBox.Name = "_equipaggioBox";
            this._equipaggioBox.Size = new System.Drawing.Size(186, 79);
            this._equipaggioBox.TabIndex = 6;
            // 
            // _camion
            // 
            this._camion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._camion.FormattingEnabled = true;
            this._camion.Location = new System.Drawing.Point(93, 46);
            this._camion.Name = "_camion";
            this._camion.Size = new System.Drawing.Size(121, 21);
            this._camion.TabIndex = 5;
            // 
            // GestioneTurnoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 427);
            this.Controls.Add(this._TurniGrid);
            this.Controls.Add(this.panel1);
            this.Name = "GestioneTurnoForm";
            this.Text = "Gestione Turni";
            ((System.ComponentModel.ISupportInitialize)(this._TurniGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView _TurniGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox _Percorso;
        private System.Windows.Forms.Button _aggiungi;
        private System.Windows.Forms.Button _rimuovi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckedListBox _equipaggioBox;
        private System.Windows.Forms.ComboBox _camion;
        private System.Windows.Forms.ComboBox _tipo;
        private System.Windows.Forms.Panel _tipoTurnoPanel;
    }
}