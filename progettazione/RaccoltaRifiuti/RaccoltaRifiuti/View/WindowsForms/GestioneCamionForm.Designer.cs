﻿namespace RaccoltaRifiuti.View.WindowsForms
{
    partial class GestioneCamionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this._camionList = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this._rimuovi = new System.Windows.Forms.Button();
            this._aggiungi = new System.Windows.Forms.Button();
            this._tipiLabel = new System.Windows.Forms.Label();
            this._tipiSupportati = new System.Windows.Forms.CheckedListBox();
            this._targa = new System.Windows.Forms.TextBox();
            this._targaLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this._camionList)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _camionList
            // 
            this._camionList.AllowUserToOrderColumns = true;
            this._camionList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._camionList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this._camionList.BackgroundColor = System.Drawing.SystemColors.Control;
            this._camionList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._camionList.DefaultCellStyle = dataGridViewCellStyle1;
            this._camionList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._camionList.Location = new System.Drawing.Point(0, 59);
            this._camionList.Name = "_camionList";
            this._camionList.RowHeadersVisible = false;
            this._camionList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._camionList.Size = new System.Drawing.Size(894, 351);
            this._camionList.TabIndex = 0;
            this._camionList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this._camionList_CellContentClick);
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this._rimuovi);
            this.panel1.Controls.Add(this._aggiungi);
            this.panel1.Controls.Add(this._tipiLabel);
            this.panel1.Controls.Add(this._tipiSupportati);
            this.panel1.Controls.Add(this._targa);
            this.panel1.Controls.Add(this._targaLabel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(894, 59);
            this.panel1.TabIndex = 1;
            // 
            // _rimuovi
            // 
            this._rimuovi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._rimuovi.Enabled = false;
            this._rimuovi.Location = new System.Drawing.Point(807, 20);
            this._rimuovi.Name = "_rimuovi";
            this._rimuovi.Size = new System.Drawing.Size(75, 23);
            this._rimuovi.TabIndex = 6;
            this._rimuovi.Text = "Rimuovi";
            this._rimuovi.UseVisualStyleBackColor = true;
            // 
            // _aggiungi
            // 
            this._aggiungi.Enabled = false;
            this._aggiungi.Location = new System.Drawing.Point(477, 20);
            this._aggiungi.Name = "_aggiungi";
            this._aggiungi.Size = new System.Drawing.Size(75, 23);
            this._aggiungi.TabIndex = 5;
            this._aggiungi.Text = "Aggiungi";
            this._aggiungi.UseVisualStyleBackColor = true;
            // 
            // _tipiLabel
            // 
            this._tipiLabel.AutoSize = true;
            this._tipiLabel.Location = new System.Drawing.Point(192, 25);
            this._tipiLabel.Name = "_tipiLabel";
            this._tipiLabel.Size = new System.Drawing.Size(76, 13);
            this._tipiLabel.TabIndex = 4;
            this._tipiLabel.Text = "Tipi supportati:";
            // 
            // _tipiSupportati
            // 
            this._tipiSupportati.CheckOnClick = true;
            this._tipiSupportati.FormattingEnabled = true;
            this._tipiSupportati.Location = new System.Drawing.Point(274, 7);
            this._tipiSupportati.Name = "_tipiSupportati";
            this._tipiSupportati.Size = new System.Drawing.Size(197, 49);
            this._tipiSupportati.TabIndex = 3;
            this._tipiSupportati.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // _targa
            // 
            this._targa.Location = new System.Drawing.Point(56, 22);
            this._targa.Name = "_targa";
            this._targa.Size = new System.Drawing.Size(130, 20);
            this._targa.TabIndex = 1;
            // 
            // _targaLabel
            // 
            this._targaLabel.AutoSize = true;
            this._targaLabel.Location = new System.Drawing.Point(12, 25);
            this._targaLabel.Name = "_targaLabel";
            this._targaLabel.Size = new System.Drawing.Size(38, 13);
            this._targaLabel.TabIndex = 0;
            this._targaLabel.Text = "Targa:";
            this._targaLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // GestioneCamionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 410);
            this.Controls.Add(this._camionList);
            this.Controls.Add(this.panel1);
            this.Name = "GestioneCamionForm";
            this.Text = "Gestione Camion";
            ((System.ComponentModel.ISupportInitialize)(this._camionList)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView _camionList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckedListBox _tipiSupportati;
        private System.Windows.Forms.TextBox _targa;
        private System.Windows.Forms.Label _targaLabel;
        private System.Windows.Forms.Button _rimuovi;
        private System.Windows.Forms.Button _aggiungi;
        private System.Windows.Forms.Label _tipiLabel;
    }
}