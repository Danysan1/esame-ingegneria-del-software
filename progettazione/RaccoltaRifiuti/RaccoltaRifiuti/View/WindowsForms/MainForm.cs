﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View.WindowsForms
{
    public partial class MainForm : Form, IMainView
    {

        public ToolStripMenuItem Gestione { get { return _gestione; } }
        public ToolStripItem Camion { get { return _camion; } }
        public ToolStripItem Persone { get { return _persone; } }
        public ToolStripItem TipiDiRifiuto { get { return _tipiDiRifiuto; } }
        public ToolStripItem Salva { get { return _salva; } }
        public ToolStripItem SalvaSu { get { return _salvaSu; } }
        public ToolStripItem CaricaDa { get { return _carica; } }
        public ToolStripItem Destinazioni { get { return _destinzioni; } }
        public ToolStripItem Percorsi { get { return _Percorsi; } }
        public ToolStripItem PuntiDiRaccolta { get { return _puntiDiRaccolta; } }
        public ToolStripItem Esci { get { return _esci; } }
        public ToolStripItem Logout { get { return _logout; } }
        public ToolStripItem Credenziali { get { return _credenziali; } }
        public DataGridView Calendario { get { return _calendario; } }

        public MainForm()
        {
            InitializeComponent();
        }
    }
}
