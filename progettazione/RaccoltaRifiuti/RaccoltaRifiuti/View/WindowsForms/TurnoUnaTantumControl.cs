﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View.WindowsForms
{
    public partial class TurnoUnaTantumControl : UserControl
    {
        public DateTimePicker Inizio { get { return _dateTime; } }

        public TurnoUnaTantumControl()
        {
            InitializeComponent();
        }
    }
}
