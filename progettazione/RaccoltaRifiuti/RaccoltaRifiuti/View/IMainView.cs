﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View
{
    public interface IMainView
    {
        event FormClosedEventHandler FormClosed;
        ToolStripMenuItem Gestione { get; }
        ToolStripItem Camion { get; }
        ToolStripItem Persone { get; }
        ToolStripItem TipiDiRifiuto { get; }
        ToolStripItem Salva { get; }
        ToolStripItem SalvaSu { get; }
        ToolStripItem CaricaDa { get; }
        ToolStripItem Destinazioni { get; }
        ToolStripItem Percorsi { get; }
        ToolStripItem PuntiDiRaccolta { get; }
        ToolStripItem Esci { get; }
        ToolStripItem Logout { get; }
        ToolStripItem Credenziali { get; }
        DataGridView Calendario { get; }
        void Show();
        void Hide();
    }
}
