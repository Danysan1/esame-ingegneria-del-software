﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View
{
    public interface ICamionView
    {
        DataGridView CamionView { get; }
        TextBox Targa { get; }
        CheckedListBox TipiSupporati { get; }
        Button Aggiungi { get; }
        Button Rimuovi { get; }
    }
}
