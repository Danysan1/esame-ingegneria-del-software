﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View
{
    public interface IPercorsiView
    {
        DataGridView DataView { get; }
        ComboBox Destinazione { get; }
        CheckedListBox PuntiDiRaccolta { get; }
        Button Aggiungi { get; }
        Button Rimuovi { get; }
        DateTimePicker DurataPrevista { get; }
    }
}
