﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View
{
    public interface IIntervalloDateOrarioView
    {
        DateTimePicker DataInizio { get; }
        DateTimePicker OraInizio { get; }
        DateTimePicker DataFine { get; }
    }
}
