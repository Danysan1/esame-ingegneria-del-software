﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.View
{
    public interface IDestinazioniView
    {
        Button Aggiungi { get; }
        Button Rimuovi { get; }
        TextBox Nome { get; }
        TextBox Indirizzo { get; }
        DataGridView DestinazioniView { get; }
    }
}
