﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Model;

namespace RaccoltaRifiuti.Persistenza
{
    public interface IPersistenza
    {
        /// <summary>
        /// Carica le destinazioni dalla persistenza.
        /// </summary>
        /// <returns>Tutte le destinazioni disponibili.</returns>
        IEnumerable<Destinazione> CaricaDestinazioni();

        /// <summary>
        /// Salva una destinazione nella persistenza.
        /// </summary>
        /// <param name="d">La destinazione da salvare.</param>
        /// <returns>Successo o fallimento.</returns>
        bool SalvaDestinazione(Destinazione d);

        /// <summary>
        /// Carica le persone dalla persistenza.
        /// </summary>
        /// <returns>Tutte le persone disponibili.</returns>
        IEnumerable<Persona> CaricaPersone();

        /// <summary>
        /// Salva una persona nella persistenza.
        /// </summary>
        /// <param name="p">La persona da salvare.</param>
        /// <returns>Successo o fallimento.</returns>
        bool SalvaPersona(Persona p);

        /// <summary>
        /// Carica i punti di raccolta dalla persistenza.
        /// </summary>
        /// <returns>Tutti i punti di raccolta disponibili.</returns>
        IEnumerable<PuntoDiRaccolta> CaricaPuntiDiRaccolta();

        /// <summary>
        /// Salva un punto di raccolta nella persistenza.
        /// </summary>
        /// <param name="p">Il punto di raccolta da salvare.</param>
        /// <returns>Successo o fallimento.</returns>
        bool SalvaPuntoDiRaccolta(PuntoDiRaccolta p);

        /// <summary>
        /// Carica i tipi di rifiuto dalla persistenza.
        /// </summary>
        /// <returns>Tutti i tipi di rifiuto disponibili.</returns>
        IEnumerable<TipoRifiuto> CaricaTipiDiRifiuto();

        /// <summary>
        /// Salva un tipo di rifiuto nella persistenza.
        /// </summary>
        /// <param name="t">Il tipo di rifiuto da salvare.</param>
        /// <returns>Successo o fallimento.</returns>
        bool SalvaTipoDiRifiuto(TipoRifiuto t);

        /// <summary>
        /// Carica i camion dalla persistenza.
        /// </summary>
        /// <param name="tipi">I tipi di rifiuto già caricati.</param>
        /// <returns>Tutti i camion disponibili.</returns>
        IEnumerable<Camion> CaricaCamion(IEnumerable<TipoRifiuto> tipi);

        /// <summary>
        /// Salva un camion nella persistenza.
        /// </summary>
        /// <param name="c">Il camion da salvare.</param>
        /// <returns>Successo o fallimento.</returns>
        bool SalvaCamion(Camion c);

        /// <summary>
        /// Carica i Percorsi dalla persistenza.
        /// </summary>
        /// <param name="destinazioni">Le destinazioni già caricate.</param>
        /// <param name="punti">I punti di raccolta già caricati.</param>
        /// <returns>Successo o fallimento.</returns>
        IEnumerable<Percorso> CaricaPercorsi(IEnumerable<Destinazione> destinazioni, IEnumerable<PuntoDiRaccolta> punti);

        /// <summary>
        /// Salva un Percorso nella persistenza.
        /// </summary>
        /// <param name="v">Il Percorso da salvare.</param>
        /// <returns>Successo o fallimento.</returns>
        bool SalvaPercorso(Percorso v);

        /// <summary>
        /// Carica i turni dalla persistenza.
        /// </summary>
        /// <param name="persone">Le persone già caricate.</param>
        /// <param name="tipi">I tipi di rifiuto già caricati.</param>
        /// <param name="camion">I camion già caricati.</param>
        /// <param name="Percorsi">I Percorsi già caricati.</param>
        /// <returns>Tutti i turni disponibili.</returns>
        IEnumerable<Turno> CaricaTurni(IEnumerable<Persona> persone, IEnumerable<TipoRifiuto> tipi, IEnumerable<Camion> camion, IEnumerable<Percorso> Percorsi);

        /// <summary>
        /// Salva un turno nella persistenza.
        /// </summary>
        /// <param name="t">Il turno da salvare.</param>
        /// <returns>Successo o fallimento.</returns>
        bool SalvaTurno(Turno t);

        bool SalvaTutto(
            IEnumerable<Destinazione> destinazioni,
            IEnumerable<Persona> persone,
            IEnumerable<PuntoDiRaccolta> punti,
            IEnumerable<TipoRifiuto> tipi,
            IEnumerable<Camion> camion,
            IEnumerable<Percorso> Percorsi,
            IEnumerable<Turno> turni
            );
    }
}
