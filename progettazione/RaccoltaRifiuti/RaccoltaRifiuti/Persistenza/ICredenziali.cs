﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Persistenza
{
    public interface ICredenziali
    {
        bool VerificaCredenziali(string codiceFiscale, string password);
        void ModificaCredenziali(string codiceFiscale, string password);

        void RimuoviCredenziali(string codiceFiscale);
    }
}
