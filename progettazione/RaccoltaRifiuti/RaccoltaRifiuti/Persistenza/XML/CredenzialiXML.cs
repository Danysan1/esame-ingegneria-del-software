﻿using RaccoltaRifiuti.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RaccoltaRifiuti.Persistenza.XML
{
    public class CredenzialiXML : BaseXML, ICredenziali
    {
        private SHA256CryptoServiceProvider _crypto;

        public CredenzialiXML(string fileName, bool append) : base(fileName, append)
        {
            _crypto = new SHA256CryptoServiceProvider();
        }

        public bool VerificaCredenziali(string codiceFiscale, string password)
        {
            if (string.IsNullOrWhiteSpace(codiceFiscale))
                throw new ArgumentException("codiceFiscale");
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentException("password");

            XmlNode n = _doc.SelectSingleNode("/Credenziali/Persona[@codiceFiscale = '" + codiceFiscale.Ripulisci() + "']/@pwdHash");

            return (n != null) && (getHash(password) == n.Value);
        }

        public void ModificaCredenziali(string codiceFiscale, string password)
        {
            if (string.IsNullOrWhiteSpace(codiceFiscale))
                throw new ArgumentException("codiceFiscale");
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentException("password");

            XmlElement x = (XmlElement)_doc.SelectSingleNode("/Credenziali");
            if (x == null)
            {
                x = _doc.CreateElement("Credenziali");
                _doc.AppendChild(x);
            }

            string cf = codiceFiscale.Ripulisci();
            XmlElement e = (XmlElement)x.SelectSingleNode("Persona[@codiceFiscale = '" + cf + "']");
            if (e == null)
            {
                e = _doc.CreateElement("Persona");
                e.SetAttribute("codiceFiscale", cf);
                x.AppendChild(e);
            }

            e.SetAttribute("pwdHash", getHash(password));
            _doc.Save(_fileName);
        }

        public void RimuoviCredenziali(string codiceFiscale)
        {
            if (string.IsNullOrWhiteSpace(codiceFiscale))
                throw new ArgumentException("codiceFiscale");

            XmlElement x = (XmlElement)_doc.SelectSingleNode("/Credenziali");
            if (x != null)
            {
                string cf = codiceFiscale.Ripulisci();
                XmlElement e = (XmlElement)x.SelectSingleNode("Persona[@codiceFiscale = '" + cf + "']");
                if (e != null)
                    x.RemoveChild(e);
            }
        }

        private string getHash(string password)
        {
            return BitConverter.ToString(_crypto.ComputeHash(Encoding.UTF8.GetBytes(password)));
        }
    }
}
