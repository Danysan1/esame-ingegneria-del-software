﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.Model.Turni;
using System.Globalization;
using RaccoltaRifiuti.Utils;

namespace RaccoltaRifiuti.Persistenza.XML.Turni
{
    class PersistenzaXMLTurnoSettimanale : IPersistenzaXMLTurno
    {
        public Type TipoAccettato { get { return typeof(TurnoSettimanale); } }

        public IEnumerable<Turno> Carica(XmlDocument doc, IEnumerable<Persona> persone, IEnumerable<TipoRifiuto> tipi, IEnumerable<Camion> camion, IEnumerable<Percorso> Percorsi)
        {
            return from XmlElement nodo
                   in doc.SelectNodes("/Model/InsiemeTurni/TurnoSettimanale[@id][@oraInizio][@dataInizio][@dataFine][@tipo][@Percorso][@camion][Partecipante[@codiceFiscale]]")
                   let id = int.Parse(nodo.GetAttribute("id"))
                   let nomeTipo = nodo.GetAttribute("tipo")
                   let idPercorso = int.Parse(nodo.GetAttribute("Percorso"))
                   let targaCamion = nodo.GetAttribute("camion").Ripulisci()
                   let oraInizio = TimeSpan.Parse(nodo.GetAttribute("oraInizio"))
                   let dataInizio = DateTime.Parse(nodo.GetAttribute("dataInizio"), DateTimeFormatInfo.InvariantInfo)
                   let dataFine = DateTime.Parse(nodo.GetAttribute("dataFine"), DateTimeFormatInfo.InvariantInfo)
                   join TipoRifiuto tipo in tipi on nomeTipo equals tipo.Nome
                   join Camion camionUtilizzato in camion on targaCamion equals camionUtilizzato.Targa
                   where camionUtilizzato.TipiSupportati.Contains(tipo)
                   join Percorso Percorso in Percorsi on idPercorso equals Percorso.Identificativo
                   let equipaggio = XMLTurno.GetEquipaggio(nodo, persone)
                   select new TurnoSettimanale(oraInizio, dataInizio, dataFine, id, Percorso, equipaggio.ToList(), camionUtilizzato, tipo);
        }

        public bool Salva(XmlDocument doc, Turno t, string nomeFile)
        {
            if (t != null && t is TurnoSettimanale)
            {
                TurnoSettimanale ts = (TurnoSettimanale)t;
                XmlElement insiemeTurni, e = XMLTurno.CreaElementoTurno(doc, "TurnoSettimanale", ts.ID, out insiemeTurni);

                try
                {
                    XMLTurno.PreparaAttributiTurno(doc, e, ts);
                    e.SetAttribute("oraInizio", ts.OrarioDiInizio.ToString());
                    e.SetAttribute("dataInizio", ts.DataDiInizio.ToString("s"));
                    e.SetAttribute("dataFine", ts.DataDiFine.ToString("s"));
                    doc.Save(nomeFile);
                    return true;
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Impossibile salvare il turno " + ts.ID + ": " + ex.Message);
                    insiemeTurni.RemoveChild(e);
                }
            }

            return false;
        }
    }
}
