﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.Model.Turni;
using System.Globalization;
using RaccoltaRifiuti.Utils;

namespace RaccoltaRifiuti.Persistenza.XML.Turni
{
    class PersistenzaXMLTurnoGiornaliero : IPersistenzaXMLTurno
    {
        public Type TipoAccettato { get { return typeof(TurnoGiornaliero); } }

        public IEnumerable<Turno> Carica(XmlDocument doc, IEnumerable<Persona> persone, IEnumerable<TipoRifiuto> tipi, IEnumerable<Camion> camion, IEnumerable<Percorso> Percorsi)
        {
            return from XmlElement nodo
                   //in doc.SelectNodes("/Model/InsiemeTurni/TurnoGiornaliero[@id and not(@id = preceding::TurnoGiornaliero/@id)][@oraInizio][@dataInizio][@dataFine][@tipo][@Percorso][@camion][Partecipante[@codiceFiscale]]") // id è primary key
                   in doc.SelectNodes("/Model/InsiemeTurni/TurnoGiornaliero[@id][@oraInizio][@dataInizio][@dataFine][@tipo][@Percorso][@camion][Partecipante[@codiceFiscale]]")
                   let id = int.Parse(nodo.GetAttribute("id"))
                   let nomeTipo = nodo.GetAttribute("tipo")
                   let idPercorso = int.Parse(nodo.GetAttribute("Percorso"))
                   let targaCamion = nodo.GetAttribute("camion").Ripulisci()
                   let oraInizio = DateTime.Parse(nodo.GetAttribute("oraInizio"), DateTimeFormatInfo.InvariantInfo)
                   let dataInizio = DateTime.Parse(nodo.GetAttribute("dataInizio"), DateTimeFormatInfo.InvariantInfo)
                   let dataFine = DateTime.Parse(nodo.GetAttribute("dataFine"), DateTimeFormatInfo.InvariantInfo)
                   join TipoRifiuto tipo in tipi on nomeTipo equals tipo.Nome
                   join Camion camionUtilizzato in camion on targaCamion equals camionUtilizzato.Targa
                   where camionUtilizzato.TipiSupportati.Contains(tipo)
                   join Percorso Percorso in Percorsi on idPercorso equals Percorso.Identificativo
                   let equipaggio = XMLTurno.GetEquipaggio(nodo, persone)
                   select new TurnoGiornaliero(oraInizio, dataInizio, dataFine, id, Percorso, equipaggio.ToList(), camionUtilizzato, tipo);
        }

        public bool Salva(XmlDocument doc, Turno t, string nomeFile)
        {
            bool result;

            if (t == null || !(t is TurnoGiornaliero))
                result = false; // Non valido
            else
            {
                TurnoGiornaliero tg = (TurnoGiornaliero)t;
                XmlElement insiemeTurni, e = XMLTurno.CreaElementoTurno(doc, "TurnoGiornaliero", tg.ID, out insiemeTurni);

                try
                {
                    XMLTurno.PreparaAttributiTurno(doc, e, tg);
                    e.SetAttribute("oraInizio", tg.OrarioDiInizio.ToString());
                    e.SetAttribute("dataInizio", tg.DataDiInizio.ToString("s"));
                    e.SetAttribute("dataFine", tg.DataDiFine.ToString("s"));
                    doc.Save(nomeFile);
                    result = true;
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Impossibile salvare il turno " + tg.ID + ": " + ex.Message);
                    insiemeTurni.RemoveChild(e);
                    result = false;
                }
            }

            return result;
        }
    }
}
