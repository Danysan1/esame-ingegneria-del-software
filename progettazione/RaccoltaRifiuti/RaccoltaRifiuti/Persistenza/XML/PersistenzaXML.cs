﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Utils;
using RaccoltaRifiuti.Model;
using System.Xml;
using System.IO;
using System.Collections;
using System.Reflection;

namespace RaccoltaRifiuti.Persistenza.XML
{
    /// <summary>
    /// Persistenza su un file XML.
    /// </summary>
    public class PersistenzaXML : BaseXML, IPersistenza
    {
        /// <summary>
        /// Cerca e carica il file XML.
        /// </summary>
        /// <param name="fileName">Percorso (assoluto o relativo) del file XML.</param>
        /// <param name="append">Non partirte da zero, appendi a un file già esistente.</param>
        /// <exception cref="XmlException">Il file è malformato</exception>
        /// <exception cref="FileNotFoundException">Il file non esiste</exception>
        public PersistenzaXML(String fileName, bool append) : base(fileName, append) { }

        public IEnumerable<Camion> CaricaCamion(IEnumerable<TipoRifiuto> tipi)
        {
            return from XmlElement nodo
                   //in _doc.SelectNodes("/Model/InsiemeCamion/Camion[@targa and not(@targa = preceding::Camion/@targa)][TipoSupportato[@nome]]") // targa è primary key
                   in _doc.SelectNodes("/Model/InsiemeCamion/Camion[@targa][TipoSupportato[@nome]]")
                   let targa = nodo.GetAttribute("targa")
                   let tipiSupportati = from XmlElement sottoNodo
                                        in nodo.SelectNodes("TipoSupportato[@nome]")
                                        let nome = sottoNodo.GetAttribute("nome")
                                        join TipoRifiuto tipo in tipi on nome equals tipo.Nome
                                        select tipo
                   select new Camion(targa, tipiSupportati.ToList());
        }

        public IEnumerable<Destinazione> CaricaDestinazioni()
        {
            return from XmlElement nodo
                   //in _doc.SelectNodes("/Model/InsiemeDestinazioni/Destinazione[@nome][@indirizzo and not(@indirizzo = preceding::Destinazione/@indirizzo)]") // indirizzo è primary key
                   in _doc.SelectNodes("/Model/InsiemeDestinazioni/Destinazione[@nome][@indirizzo]")
                   let nome = nodo.GetAttribute("nome")
                   let indirizzo = nodo.GetAttribute("indirizzo")
                   select new Destinazione(nome, indirizzo);
        }

        public IEnumerable<Persona> CaricaPersone()
        {
            return from XmlElement nodo
                   //in _doc.SelectNodes("/Model/InsiemePersone/Persona[@nome][@cognome][@codiceFiscale and not(@codiceFiscale = preceding::Persona/@codiceFiscale)]") // codiceFiscale è primary key
                   in _doc.SelectNodes("/Model/InsiemePersone/Persona[@nome][@cognome][@codiceFiscale]")
                   let nome = nodo.GetAttribute("nome")
                   let cognome = nodo.GetAttribute("cognome")
                   let codiceFiscale = nodo.GetAttribute("codiceFiscale")
                   let amm = nodo.GetAttribute("amministratore")
                   let amministratore = "si" == amm || "1" == amm // Se amministratore non è presente il controllo sarà ("si" == null), quindi false
                   select new Persona(nome, cognome, codiceFiscale, amministratore);
        }

        public IEnumerable<PuntoDiRaccolta> CaricaPuntiDiRaccolta()
        {
            return from XmlElement nodo
                   //in _doc.SelectNodes("/Model/InsiemePuntiDiRaccolta/Punto[@nome and not(@nome = preceding::Punto/@nome)][@indirizzo]") // nome è primary key
                   in _doc.SelectNodes("/Model/InsiemePuntiDiRaccolta/Punto[@nome][@indirizzo]")
                   let nome = nodo.GetAttribute("nome")
                   let indirizzo = nodo.GetAttribute("indirizzo")
                   select new PuntoDiRaccolta(nome, indirizzo);
        }

        public IEnumerable<TipoRifiuto> CaricaTipiDiRifiuto()
        {
            return from XmlElement nodo
                   //in _doc.SelectNodes("/Model/InsiemeTipiDiRifiuto/Tipo[@nome and not(@nome = preceding::Tipo/@nome)]") // nome è primary key
                   in _doc.SelectNodes("/Model/InsiemeTipiDiRifiuto/Tipo[@nome]")
                   let nome = nodo.GetAttribute("nome")
                   select new TipoRifiuto(nome);
        }

        public IEnumerable<Turno> CaricaTurni(IEnumerable<Persona> persone, IEnumerable<TipoRifiuto> tipi, IEnumerable<Camion> camion, IEnumerable<Percorso> Percorsi)
        {
            List<Turno> list = new List<Turno>();

            foreach (IPersistenzaXMLTurno obj in from Type type in Assembly.GetExecutingAssembly().GetTypes()
                                                 where !type.IsAbstract && typeof(IPersistenzaXMLTurno).IsAssignableFrom(type) // Classe concreta che implementa IPersistenzaXMLTurno
                                                 let contructor = type.GetConstructor(Type.EmptyTypes)
                                                 where contructor != null // Ha un costruttore senza argomenti
                                                 select (IPersistenzaXMLTurno)contructor.Invoke(null))
                list.AddRange(obj.Carica(_doc, persone, tipi, camion, Percorsi));

            return list;
        }

        public IEnumerable<Percorso> CaricaPercorsi(IEnumerable<Destinazione> destinazioni, IEnumerable<PuntoDiRaccolta> punti)
        {
            return from XmlElement nodo
                   //in _doc.SelectNodes("/Model/InsiemePercorsi/Percorso[@id and not(@id = preceding::Percorso/@id)][@durata][@destinazione][Fermata[@nome]]") // id è primary key
                   in _doc.SelectNodes("/Model/InsiemePercorsi/Percorso[@id][@durata][@destinazione][Fermata[@nome]]")
                   let indirizzoDest = nodo.GetAttribute("destinazione")
                   let durataPrevista = TimeSpan.Parse(nodo.GetAttribute("durata"))
                   let id = int.Parse(nodo.GetAttribute("id"))
                   join Destinazione destinazione in destinazioni on indirizzoDest equals destinazione.Indirizzo
                   let puntiDiRaccolta = from XmlElement sottoNodo
                                         in nodo.SelectNodes("Fermata[@nome]")
                                         let nome = sottoNodo.GetAttribute("nome")
                                         join PuntoDiRaccolta punto in punti on nome equals punto.Nome
                                         select punto
                   select new Percorso(durataPrevista, id, destinazione, puntiDiRaccolta.ToList());
        }

        public bool SalvaCamion(Camion c)
        {
            bool result;

            if (c == null)
                result = false; // Nullo, non può essere inserito
            else
            {
                XmlElement insiemeCamion = _doc.OttieniInsieme("InsiemeCamion"),
                    e = (XmlElement)_doc.SelectSingleNode("/Model/InsiemeCamion/Camion[@targa='" + c.Targa + "']");
                if (e != null) // Esiste già
                    e.RemoveAll(); // Rimuovi tutti gli attributi e figli
                else // L'elemento per il camion con questa targa non esiste ancora
                {
                    e = _doc.CreateElement("Camion");
                    insiemeCamion.AppendChild(e);
                }

                try
                {
                    e.SetAttribute("targa", c.Targa);
                    foreach (TipoRifiuto tipo in c.TipiSupportati)
                    {
                        XmlElement sup = _doc.CreateElement("TipoSupportato");
                        sup.SetAttribute("nome", tipo.Nome);
                        e.AppendChild(sup);
                    }
                    _doc.Save(_fileName);
                    result = true; // Successo
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Impossibile salvare il camion" + c.Targa + ": " + ex.Message);
                    insiemeCamion.RemoveChild(e);
                    result = false; // Fallimento
                }
            }

            return result;
        }

        public bool SalvaDestinazione(Destinazione d)
        {
            bool result;

            if (d == null)
                result = false; // Non valido
            else
            {
                XmlElement insiemeDestinazioni = _doc.OttieniInsieme("InsiemeDestinazioni"),
                    e = (XmlElement)_doc.SelectSingleNode("/Model/InsiemeDestinazioni/Destinazione[@indirizzo = '" + d.Indirizzo + "']");
                if (e != null)
                    e.RemoveAll();
                else // L'elemento per la destinazione con queto indirizzo non esiste ancora
                {
                    e = _doc.CreateElement("Destinazione");
                    insiemeDestinazioni.AppendChild(e);
                }

                try
                {
                    e.SetAttribute("indirizzo", d.Indirizzo);
                    e.SetAttribute("nome", d.Nome);
                    _doc.Save(_fileName);
                    result = true;
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Impossibile salvare la destinazione " + d.Indirizzo + ": " + ex.Message);
                    insiemeDestinazioni.RemoveChild(e);
                    result = false;
                }
            }

            return result;
        }

        public bool SalvaPersona(Persona p)
        {
            bool result;

            if (p == null)
                result = false; // Non valido
            else
            {
                XmlElement insiemePersone = _doc.OttieniInsieme("InsiemePersone"),
                    e = (XmlElement)_doc.SelectSingleNode("/Model/InsiemePersone/Persona[@codiceFiscale = '" + p.CodiceFiscale + "']");
                if (e != null)
                    e.RemoveAll();
                else // L'elemento per la persona con questo codice fiscale non esiste ancora
                {
                    e = _doc.CreateElement("Persona");
                    insiemePersone.AppendChild(e);
                }

                try
                {
                    e.SetAttribute("codiceFiscale", p.CodiceFiscale);
                    e.SetAttribute("nome", p.Nome);
                    e.SetAttribute("cognome", p.Cognome);
                    e.SetAttribute("amministratore", p.IsAmministratore ? "1" : "0");
                    _doc.Save(_fileName);
                    result = true;
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Impossibile salvare la persona " + p.CodiceFiscale + ": " + ex.Message);
                    insiemePersone.RemoveChild(e);
                    result = false;
                }
            }

            return result;
        }

        public bool SalvaPuntoDiRaccolta(PuntoDiRaccolta p)
        {
            bool result;

            if (p == null)
                result = false; // Non valido
            else
            {
                XmlElement insiemePuntiDiRaccolta = _doc.OttieniInsieme("InsiemePuntiDiRaccolta"),
                    e = (XmlElement)_doc.SelectSingleNode("/Model/InsiemePuntiDiRaccolta/Punto[@nome = '" + p.Nome + "']");
                if (e != null)
                    e.RemoveAll();
                else // L'elemento per il punto di raccolta con questo nome non esiste ancora
                {
                    e = _doc.CreateElement("Punto");
                    insiemePuntiDiRaccolta.AppendChild(e);
                }

                try
                {
                    e.SetAttribute("nome", p.Nome);
                    e.SetAttribute("indirizzo", p.Via);
                    _doc.Save(_fileName);
                    result = true;
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Impossibile salvare il punto di raccolta " + p.Nome + ": " + ex.Message);
                    insiemePuntiDiRaccolta.RemoveChild(e);
                    result = false;
                }
            }

            return result;
        }

        public bool SalvaTipoDiRifiuto(TipoRifiuto t)
        {
            bool result;

            if (t == null)
                result = false; // Non valido
            else if (_doc.SelectSingleNode("/Model/InsiemeTipiDiRifiuto/Tipo[@nome='" + t.Nome + "']") != null)
                result = true; // Esiste già
            else // Non esiste
            {
                XmlElement insiemeTipiDiRifiuto = _doc.OttieniInsieme("InsiemeTipiDiRifiuto"),
                    e = _doc.CreateElement("Tipo");
                try
                {
                    e.SetAttribute("nome", t.Nome);
                    insiemeTipiDiRifiuto.AppendChild(e);
                    _doc.Save(_fileName);
                    result = true;
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Impossibile salvare il tipo di rifiuto " + t.Nome + ": " + ex.Message);
                    insiemeTipiDiRifiuto.RemoveChild(e);
                    result = false;
                }
            }

            return result;
        }

        public bool SalvaTurno(Turno t)
        {
            if (t == null)
                return false;

            // Cerca la classe che implementa PersistenzaXMLTurno appropriata per il tipo di turno corrente
            foreach (IPersistenzaXMLTurno obj in from Type type in Assembly.GetExecutingAssembly().GetTypes()
                                                 let contructor = type.GetConstructor(Type.EmptyTypes)
                                                 where !type.IsAbstract && typeof(IPersistenzaXMLTurno).IsAssignableFrom(type) && contructor != null
                                                 select (IPersistenzaXMLTurno)contructor.Invoke(null))
                if (obj.TipoAccettato == t.GetType() && obj.Salva(_doc, t, _fileName))
                    return true;

            return false;
        }

        public bool SalvaPercorso(Percorso v)
        {
            bool result;

            if (v == null)
                result = false; // Non valido
            else
            {
                XmlElement insiemePercorsi = _doc.OttieniInsieme("InsiemePercorsi"),
                    e = (XmlElement)_doc.SelectSingleNode("/Model/InsiemePercorsi/Percorso[@id = '" + v.Identificativo + "']");
                if (e != null)
                    e.RemoveAll();
                else // L'elemento per il Percorso con questo id non esiste ancora
                {
                    e = _doc.CreateElement("Percorso");
                    insiemePercorsi.AppendChild(e);
                }

                try
                {
                    e.SetAttribute("id", v.Identificativo.ToString());
                    e.SetAttribute("destinazione", v.Dest.Indirizzo);
                    e.SetAttribute("durata", v.DurataPrevista.ToString());
                    foreach (PuntoDiRaccolta p in v.Itinerario)
                    {
                        XmlElement fermata = _doc.CreateElement("Fermata");
                        fermata.SetAttribute("nome", p.Nome);
                        e.AppendChild(fermata);
                    }
                    _doc.Save(_fileName);
                    result = true;
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Impossibile salvare il Percorso " + v.Identificativo + ": " + ex.Message);
                    insiemePercorsi.RemoveChild(e);
                    result = false;
                }
            }

            return result;
        }

        public bool SalvaTutto(IEnumerable<Destinazione> destinazioni,
            IEnumerable<Persona> persone,
            IEnumerable<PuntoDiRaccolta> punti,
            IEnumerable<TipoRifiuto> tipi,
            IEnumerable<Camion> camion,
            IEnumerable<Percorso> Percorsi,
            IEnumerable<Turno> turni)
        {
            _doc.RemoveAll();

            bool result = true;

            foreach (Destinazione d in destinazioni)
                result &= SalvaDestinazione(d);
            foreach (Persona p in persone)
                result &= SalvaPersona(p);
            foreach (PuntoDiRaccolta p in punti)
                result &= SalvaPuntoDiRaccolta(p);
            foreach (TipoRifiuto t in tipi)
                result &= SalvaTipoDiRifiuto(t);
            foreach (Camion c in camion)
                result &= SalvaCamion(c);
            foreach (Percorso v in Percorsi)
                result &= SalvaPercorso(v);
            foreach (Turno t in turni)
                result &= SalvaTurno(t);

            return result;
        }

        public override string ToString()
        {
            return _doc.OuterXml;
        }
    }
}
