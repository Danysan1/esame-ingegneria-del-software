﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Model;
using System.ComponentModel;

namespace RaccoltaRifiuti.Presenter
{
    public interface IDestinazioni
    {
        BindingList<Destinazione> Destinazioni { get; }

        void AggiungiDestinazione(string nome, string indirizzo);
        void AggiungiDestinazione(Destinazione d);
    }
}
