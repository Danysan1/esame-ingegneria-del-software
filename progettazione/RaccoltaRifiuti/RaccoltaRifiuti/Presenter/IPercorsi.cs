﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Model;
using System.ComponentModel;

namespace RaccoltaRifiuti.Presenter
{
    public interface IPercorsi
    {
        BindingList<Destinazione> Destinazioni { get; }
        BindingList<PuntoDiRaccolta> PuntiDiRaccolta { get; }
        BindingList<Percorso> Percorsi { get; }

        void AggiungiPercorso(TimeSpan durata, Destinazione dest, ICollection<PuntoDiRaccolta> itinerario);
        void AggiungiPercorso(Percorso v);
        int NuovoIdPercorso();
    }
}
