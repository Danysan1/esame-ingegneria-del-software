﻿using RaccoltaRifiuti.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Presenter.Dati
{
    class DettagliTurno : IComparable<DettagliTurno>
    {

        public DateTime Inizio { get; private set; }
        public DateTime Fine { get; private set; }
        public string Camion { get; private set; }
        public string PuntiDiRaccolta { get; private set; }
        public string Destinazione { get; private set; }
        public string Equipaggio { get; private set; }
        public string Rifiuto { get; private set; }


        public DettagliTurno(DateTime inizio, DateTime fine, string camion, string puntiDiRaccolta, string destinazione, string equipaggio, string rifiuto)
        {
            if (inizio == null)
                throw new ArgumentNullException("inizio");
            if (fine == null)
                throw new ArgumentNullException("fine");
            if (string.IsNullOrWhiteSpace(camion))
                throw new ArgumentException("camion");
            if (string.IsNullOrWhiteSpace(puntiDiRaccolta))
                throw new ArgumentException("puntiDiRaccolta");
            if (string.IsNullOrWhiteSpace(destinazione))
                throw new ArgumentException("destinazione");
            if (string.IsNullOrWhiteSpace(equipaggio))
                throw new ArgumentException("equipaggio");
            if (string.IsNullOrWhiteSpace(rifiuto))
                throw new ArgumentException("rifiuto");

            Inizio = inizio;
            Fine = fine;
            Camion = camion;
            PuntiDiRaccolta = puntiDiRaccolta;
            Destinazione = destinazione;
            Equipaggio = equipaggio;
            Rifiuto = rifiuto;
        }

        public int CompareTo(DettagliTurno other)
        {
            if (other == null)
                return 1;

            return Inizio.CompareTo(other.Inizio);
        }
    }
}
