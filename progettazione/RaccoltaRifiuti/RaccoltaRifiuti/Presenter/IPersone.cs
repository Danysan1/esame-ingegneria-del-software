﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Model;
using System.ComponentModel;

namespace RaccoltaRifiuti.Presenter
{
    public interface IPersone
    {
        BindingList<Persona> Persone { get; }

        void AggiungiPersona(string codiceFiscale, string nome, string cognome);
        void AggiungiPersona(Persona p);
    }
}
