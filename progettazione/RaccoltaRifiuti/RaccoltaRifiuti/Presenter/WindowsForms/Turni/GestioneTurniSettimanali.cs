﻿using RaccoltaRifiuti.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.Model.Turni;
using System.Windows.Forms;

namespace RaccoltaRifiuti.Presenter.WindowsForms.Turni
{
    class GestioneTurniSettimanali : GestioneTurniIntervalloDateOrario
    {
        public static string Titolo { get { return "Turni settimanali"; } }

        public GestioneTurniSettimanali(ITurniView gest, ITurni mod) : base(gest, mod) { }

        protected override void SincronizzaLista(object sender, ListChangedEventArgs e)
        {
            View.GridTurno.DataSource = new BindingList<TurnoSettimanale>(Mod.Turni.OfType<TurnoSettimanale>().ToList());
        }

        protected override void Aggiungi_Click(object sender, EventArgs e)
        {
            try
            {
                Mod.AggiungiTurno(new TurnoSettimanale(Ido.OraInizio.Value.TimeOfDay, Ido.DataInizio.Value, Ido.DataFine.Value, Mod.NuovoIdTurno(), (Percorso)View.Percorso.SelectedItem, Enumerable.ToList<Persona>(View.Equipaggio.CheckedItems.OfType<Persona>()), (Camion)View.Camion.SelectedItem, (TipoRifiuto)View.Tipo.SelectedItem));
            }
            catch (Exception exc)
            {
                MessageBox.Show("Parametri non validi: " + exc.Message, "Errore nell'inserimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
