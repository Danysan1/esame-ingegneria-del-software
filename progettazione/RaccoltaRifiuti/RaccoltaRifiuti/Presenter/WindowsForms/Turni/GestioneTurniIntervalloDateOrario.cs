﻿using RaccoltaRifiuti.View;
using RaccoltaRifiuti.View.WindowsForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Presenter.WindowsForms.Turni
{
    public abstract class GestioneTurniIntervalloDateOrario : GestioneTurni
    {

        protected IIntervalloDateOrarioView Ido { get; private set; }

        public GestioneTurniIntervalloDateOrario(ITurniView gest, ITurni mod) : base(gest, mod)
        {
            // Prepara il controllo
            TurnoGiornalieroControl tgc = new TurnoGiornalieroControl();
            View.PanelTurno.Controls.Add(tgc);
            Ido = tgc;

            // Aggancia gli handler degli input
            Ido.DataInizio.ValueChanged += SelectedIndexChanged;
            Ido.DataFine.ValueChanged += SelectedIndexChanged;
            Ido.DataInizio.Value = DateTime.Now;
            Ido.DataFine.Value = DateTime.Now;
            Ido.OraInizio.Value = DateTime.Now;
        }

        protected override void SelectedIndexChanged(object sender, EventArgs e)
        {
            View.Aggiungi.Enabled = SelectedIndexChangedHelper() && Ido.DataInizio.Value < Ido.DataFine.Value;
        }
    }
}
