﻿using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.Presenter.WindowsForms.Turni
{
    public abstract class GestioneTurni : GestioneTurniBase
    {
        private IEnumerable<Turno> TurniSelezionati { get { return from DataGridViewRow riga in View.GridTurno.SelectedRows select (Turno)riga.DataBoundItem; } }

        public GestioneTurni(ITurniView gest, ITurni mod) : base(gest, mod)
        {
            // Aggancia gli handler degli input
            gest.Aggiungi.Click += Aggiungi_Click;
            gest.Rimuovi.Click += Rimuovi_Click;
            gest.Percorso.SelectedIndexChanged += SelectedIndexChanged;
            gest.Camion.SelectedIndexChanged += SelectedIndexChanged;
            gest.Tipo.SelectedIndexChanged += SelectedIndexChanged;
            gest.Equipaggio.MouseUp += SelectedIndexChanged;

            // Prepara la DataGridView (le sottoclassi concrete provvederanno a gestire la DataSource)
            gest.GridTurno.SelectionChanged += GridTurno_SelectionChanged;
            gest.GridTurno.CellFormatting += (s, e) => { e.Value = (e.Value is List<Persona>) ? string.Join(Environment.NewLine, from p in (List<Persona>)e.Value select p.CodiceFiscale) : e.Value.ToString(); };

            // Prepara le ComboBox e le CheckedListBox
            foreach (Percorso v in mod.Percorsi)
                gest.Percorso.Items.Add(v);
            foreach (TipoRifiuto t in mod.TipiDiRifiuto)
                gest.Tipo.Items.Add(t);
            foreach (Camion c in mod.Camion)
                gest.Camion.Items.Add(c);
            foreach (Persona p in mod.Persone)
                gest.Equipaggio.Items.Add(p);

            // Prepara la DataGridView
            mod.Turni.ListChanged += SincronizzaLista;
            SincronizzaLista(null, null);
        }

        protected abstract void SincronizzaLista(object sender, ListChangedEventArgs e);

        void GridTurno_SelectionChanged(object sender, EventArgs e)
        {
            View.Rimuovi.Enabled = View.GridTurno.SelectedRows.Count > 0;
        }

        protected abstract void SelectedIndexChanged(object sender, EventArgs e);

        protected bool SelectedIndexChangedHelper()
        {
            return View.Percorso.SelectedItem != null && View.Camion.SelectedItem != null && View.Tipo.SelectedItem != null
                && View.Equipaggio.CheckedItems.Count > 0;
        }


        protected void Rimuovi_Click(object sender, EventArgs e)
        {
            foreach (Turno t in TurniSelezionati)
            {
                Mod.Turni.Remove(t);
            }
        }

        protected abstract void Aggiungi_Click(object sender, EventArgs e);



    }
}
