﻿using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.Presenter.WindowsForms
{
    class GestioneDestinazioni
    {
        private IDestinazioniView _form;
        private IDestinazioni _modello;

        private IEnumerable<Destinazione> DestinazioniSelezionate
        {
            get
            {
                return from DataGridViewRow row
                       in _form.DestinazioniView.SelectedRows
                       select (Destinazione)row.DataBoundItem;
            }
        }

        public GestioneDestinazioni(IDestinazioniView form, IDestinazioni modello)
        {
            if (form == null)
                throw new ArgumentNullException("form");

            if (modello == null)
                throw new ArgumentNullException("modello");

            _form = form;
            _modello = modello;

            // Aggancia gli handler degli input
            _form.Aggiungi.Click += Aggiungi_Click;
            _form.Rimuovi.Click += Rimuovi_Click;
            _form.Nome.TextChanged += TextChanged;
            _form.Indirizzo.TextChanged += TextChanged;

            // Prepara la DataGridView
            _form.DestinazioniView.SelectionChanged += DestinazioniView_SelectionChanged;
            _form.DestinazioniView.DataSource = _modello.Destinazioni;
        }

        private void DestinazioniView_SelectionChanged(object sender, EventArgs e)
        {
            _form.Rimuovi.Enabled = _form.DestinazioniView.SelectedRows.Count > 0;
        }

        private void TextChanged(object sender, EventArgs e)
        {
            _form.Aggiungi.Enabled = !string.IsNullOrWhiteSpace(_form.Nome.Text) && !string.IsNullOrWhiteSpace(_form.Indirizzo.Text);
        }

        private void Rimuovi_Click(object sender, EventArgs e)
        {
            foreach (Destinazione d in DestinazioniSelezionate)
                _modello.Destinazioni.Remove(d);
        }

        private void Aggiungi_Click(object sender, EventArgs e)
        {
            try
            {
                _modello.AggiungiDestinazione(_form.Nome.Text, _form.Indirizzo.Text);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Parametri non validi: " + exc.Message, "Errore nell'inserimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
