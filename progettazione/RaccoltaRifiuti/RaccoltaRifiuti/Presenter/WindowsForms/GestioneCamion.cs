﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.View;
using System.Windows.Forms;
using RaccoltaRifiuti.Model;

namespace RaccoltaRifiuti.Presenter.WindowsForms
{
    class GestioneCamion
    {
        private ICamionView _form;
        private ICamions _modello;

        private IEnumerable<Camion> CamionSelezionati { get { return from DataGridViewRow riga in _form.CamionView.SelectedRows select (Camion)riga.DataBoundItem; } }

        public GestioneCamion(ICamionView form, ICamions mod)
        {
            if (form == null)
                throw new ArgumentNullException("form");

            if (mod == null)
                throw new ArgumentNullException("modello");

            _form = form;
            _modello = mod;

            // Riempi la lista dei tipi supportabili
            for (int i = 0; i < mod.TipiDiRifiuto.Count; i++)
                form.TipiSupporati.Items.Insert(i, mod.TipiDiRifiuto.ElementAt(i));

            // Aggancia gli handler degli input
            _form.Aggiungi.Click += Aggiungi_Click;
            _form.Rimuovi.Click += Rimuovi_Click;
            _form.Targa.TextChanged += TextChanged;
            _form.TipiSupporati.MouseUp += TextChanged;

            // Prepara la DataGridView
            _form.CamionView.SelectionChanged += CamionView_SelectionChanged;
            form.CamionView.DataSource = mod.Camion;
            form.CamionView.CellFormatting += (s, e) => { e.Value = (e.Value is List<TipoRifiuto>) ? string.Join(Environment.NewLine, from t in (List<TipoRifiuto>)e.Value select t.Nome) : e.Value.ToString(); };
        }

        private void CamionView_SelectionChanged(object sender, EventArgs e)
        {
            _form.Rimuovi.Enabled = _form.CamionView.SelectedRows.Count > 0;
        }

        private void TextChanged(object sender, EventArgs e)
        {
            _form.Aggiungi.Enabled = !string.IsNullOrWhiteSpace(_form.Targa.Text) && _form.TipiSupporati.CheckedItems.Count > 0;
        }

        private void Rimuovi_Click(object sender, EventArgs e)
        {
            foreach (Camion c in CamionSelezionati)
                _modello.Camion.Remove(c);
        }

        private void Aggiungi_Click(object sender, EventArgs e)
        {
            try
            {
                _modello.AggiungiCamion(_form.Targa.Text, _form.TipiSupporati.CheckedItems.OfType<TipoRifiuto>().ToList());
            }
            catch (Exception exc)
            {
                MessageBox.Show("Parametri non validi: " + exc.Message, "Errore nell'inserimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
