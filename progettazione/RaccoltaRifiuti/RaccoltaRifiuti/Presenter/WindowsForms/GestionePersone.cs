﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.View;
using System.Reflection;

namespace RaccoltaRifiuti.Presenter.WindowsForms
{
    public class GestionePersone
    {
        private readonly IPersone _modello;
        private readonly IPersoneView _form;
        private Persona _utente;

        private IEnumerable<Persona> PersoneSelezionate
        {
            get
            {
                return from DataGridViewRow riga
                       in _form.Utenti.SelectedRows
                       let p = (Persona)riga.DataBoundItem
                       where p != null
                       select p;
            }
        }

        public GestionePersone(IPersoneView form, IPersone modello, Persona utente)
        {
            if (form == null)
                throw new ArgumentNullException("form");

            if (modello == null)
                throw new ArgumentNullException("modello");
            if (utente == null)
                throw new ArgumentNullException("utente");
            if (utente.IsAmministratore == false)
                throw new ArgumentException("utente");

            _form = form;
            _modello = modello;
            _utente = utente;

            // Aggancia gli handler degli input
            form.AggiungiUtente.Click += Aggiungi_Click;
            form.RimuoviUtente.Click += Rimuovi_Click;
            form.NomeUtente.TextChanged += OnTextChanged;
            form.CognomeUtente.TextChanged += OnTextChanged;
            form.CodiceFiscaleUtente.TextChanged += OnTextChanged;

            // Prepara la DataGridView
            form.Utenti.SelectionChanged += OnSelectionChanged;
            form.Utenti.DataSource = _modello.Persone;

            // Aggancia l'handler che controlla la validità delle modifiche alle persone
            _modello.Persone.ListChanged += Persone_ListChanged;
        }

        private void Persone_ListChanged(object sender, ListChangedEventArgs e)
        {
            if ((e.ListChangedType == ListChangedType.ItemChanged) && (e.PropertyDescriptor.Name == "IsAmministratore") && (_modello.Persone.ElementAt(e.NewIndex).IsAmministratore == false))
                if (!_modello.Persone.Any(persona => persona.IsAmministratore))
                    _modello.Persone.ElementAt(e.NewIndex).IsAmministratore = true;
        }

        /// Controlla se il pulsante Rimuovi vanno abilitati o disabilitati. 
        private void OnSelectionChanged(object sender, EventArgs e)
        {
            _form.RimuoviUtente.Enabled = _form.Utenti.SelectedRows.Count > 0 && !PersoneSelezionate.Contains(_utente); // Almeno un utente è selezionato e l'utente attuale non è selezionato => pulsante Rimuovi abilitato
        }

        /// Controlla se il pulsante Aggiungi va abilitato o disabilitato.
        private void OnTextChanged(object sender, EventArgs e)
        {
            _form.AggiungiUtente.Enabled = !string.IsNullOrWhiteSpace(_form.NomeUtente.Text) && !string.IsNullOrWhiteSpace(_form.CognomeUtente.Text) && !string.IsNullOrWhiteSpace(_form.CodiceFiscaleUtente.Text); // Tutti i campi testo non vuoti
        }

        /// L'utente ha cliccato Rimuovi. Rimuovere gli utenti selezionati. 
        private void Rimuovi_Click(object sender, EventArgs e)
        {
            foreach (Persona p in PersoneSelezionate)
                _modello.Persone.Remove(p);
        }

        /// L'utente ha cliccato Aggiungi. Aggiungere l'utente specificato. 
        private void Aggiungi_Click(object sender, EventArgs e)
        {
            try
            {
                _modello.AggiungiPersona(_form.CodiceFiscaleUtente.Text, _form.NomeUtente.Text, _form.CognomeUtente.Text);
            }
            catch (Exception exc) // Un argomento dell'utente non era valido
            {
                MessageBox.Show("Parametri non validi: " + exc.Message, "Errore nell'inserimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
