﻿using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.Presenter.WindowsForms
{
    class GestionePuntiDiRaccolta
    {
        private IPuntiDiRaccoltaView _form;
        private IPuntiDiRaccolta _modello;
        private IEnumerable<PuntoDiRaccolta> PuntiDiRaccoltaSelezionate
        {
            get
            {
                return from DataGridViewRow row
                       in _form.PuntiDiRaccoltaView.SelectedRows
                       select (PuntoDiRaccolta)row.DataBoundItem;
            }
        }

        public GestionePuntiDiRaccolta(IPuntiDiRaccoltaView form, IPuntiDiRaccolta modello)
        {
            if (form == null)
                throw new ArgumentNullException("form");

            if (modello == null)
                throw new ArgumentNullException("modello");

            _form = form;
            _modello = modello;

            _form.Text = "Gestione Punti di Raccolta";

            // Aggancia gli handler degli input
            _form.Aggiungi.Click += Aggiungi_Click;
            _form.Rimuovi.Click += Rimuovi_Click;
            _form.Nome.TextChanged += TextChanged;
            _form.Indirizzo.TextChanged += TextChanged;

            // Prepara la DataGridView
            _form.PuntiDiRaccoltaView.SelectionChanged += DestinazioniView_SelectionChanged;
            _form.PuntiDiRaccoltaView.DataSource = _modello.PuntiDiRaccolta;
        }

        private void DestinazioniView_SelectionChanged(object sender, EventArgs e)
        {
            _form.Rimuovi.Enabled = _form.PuntiDiRaccoltaView.SelectedRows.Count > 0;
        }

        private void TextChanged(object sender, EventArgs e)
        {
            _form.Aggiungi.Enabled = !string.IsNullOrWhiteSpace(_form.Nome.Text) && !string.IsNullOrWhiteSpace(_form.Indirizzo.Text);
        }

        private void Rimuovi_Click(object sender, EventArgs e)
        {
            foreach (PuntoDiRaccolta p in PuntiDiRaccoltaSelezionate)
                _modello.PuntiDiRaccolta.Remove(p);
        }

        private void Aggiungi_Click(object sender, EventArgs e)
        {
            try
            {
                _modello.AggiungiPuntoDiRaccolta(_form.Nome.Text, _form.Indirizzo.Text);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Parametri non validi: " + exc.Message, "Errore nell'inserimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
