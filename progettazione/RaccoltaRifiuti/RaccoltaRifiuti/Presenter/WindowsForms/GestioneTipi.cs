﻿using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaccoltaRifiuti.Presenter.WindowsForms
{
    class GestioneTipi
    {
        private ITipiDiRifiutoView _gest;
        private ITipiDiRifiuto _gestMod;
        private IEnumerable<TipoRifiuto> TipiSelezionati { get { return from DataGridViewRow riga in _gest.TipiView.SelectedRows select (TipoRifiuto)riga.DataBoundItem; } }

        public GestioneTipi(ITipiDiRifiutoView gtf, ITipiDiRifiuto gm)
        {
            if (gtf == null)
                throw new ArgumentNullException("view");
            if (gm == null)
                throw new ArgumentNullException("model");

            this._gest = gtf;
            this._gestMod = gm;

            // Aggancia gli handler degli input
            gtf.Nome.TextChanged += Nome_TextChanged;
            gtf.Aggiungi.Click += Aggiungi_Click;
            gtf.Rimuovi.Click += Rimuovi_Click;

            // Prepara la DataGridView
            gtf.TipiView.SelectionChanged += TipiView_SelectionChanged;
            gtf.TipiView.DataSource = gm.TipiDiRifiuto;
        }

        void TipiView_SelectionChanged(object sender, EventArgs e)
        {
            _gest.Rimuovi.Enabled = _gest.TipiView.SelectedRows.Count > 0;
        }

        void Nome_TextChanged(object sender, EventArgs e)
        {
            _gest.Aggiungi.Enabled = !string.IsNullOrWhiteSpace(_gest.Nome.Text);
        }

        void Rimuovi_Click(object sender, EventArgs e)
        {
            foreach (TipoRifiuto tipo in TipiSelezionati)
                _gestMod.TipiDiRifiuto.Remove(tipo);
        }

        void Aggiungi_Click(object sender, EventArgs e)
        {
            try
            {
                _gestMod.AggiungiTipoDiRifiuto(_gest.Nome.Text);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Parametri non validi: " + exc.Message, "Errore nell'inserimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
