﻿using RaccoltaRifiuti.Persistenza;
using RaccoltaRifiuti.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Presenter.WindowsForms
{
    class GestioneCredenziali
    {
        private ICredenzialiView _cv;
        private ICredenziali _c;

        public GestioneCredenziali(ICredenzialiView view, ICredenziali credenziali, IList<string> utenti)
        {
            if (view == null)
                throw new ArgumentNullException("view");
            _cv = view;

            if (credenziali == null)
                throw new ArgumentNullException("credenziali");
            _c = credenziali;

            if (utenti == null)
                throw new ArgumentNullException("utenti");

            view.Utente.DataSource = utenti;
            view.Utente.SelectedValueChanged += SelectedValueChanged;
            view.Password.TextChanged += SelectedValueChanged;
            view.Conferma.Click += Conferma_Click;
            view.Rimuovi.Click += Rimuovi_Click;
        }

        private void Rimuovi_Click(object sender, EventArgs e)
        {
            _c.RimuoviCredenziali((string)_cv.Utente.SelectedValue);
            _cv.Password.ResetText();
        }

        private void Conferma_Click(object sender, EventArgs e)
        {
            _c.ModificaCredenziali((string)_cv.Utente.SelectedValue, _cv.Password.Text);
            _cv.Password.ResetText();
        }

        private void SelectedValueChanged(object sender, EventArgs e)
        {
            _cv.Conferma.Enabled = _cv.Utente.SelectedValue != null && !string.IsNullOrWhiteSpace(_cv.Password.Text);
            _cv.Rimuovi.Enabled = _cv.Utente.SelectedValue != null;
        }
    }
}
