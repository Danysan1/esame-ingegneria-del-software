﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Model;
using System.ComponentModel;

namespace RaccoltaRifiuti.Presenter
{
    public interface ITipiDiRifiuto
    {
        BindingList<TipoRifiuto> TipiDiRifiuto { get; }

        void AggiungiTipoDiRifiuto(string nome);
        void AggiungiTipoDiRifiuto(TipoRifiuto t);
    }
}
