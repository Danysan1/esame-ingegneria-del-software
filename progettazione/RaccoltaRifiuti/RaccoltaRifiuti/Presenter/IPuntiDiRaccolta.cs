﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Model;
using System.ComponentModel;

namespace RaccoltaRifiuti.Presenter
{
    public interface IPuntiDiRaccolta
    {
        BindingList<PuntoDiRaccolta> PuntiDiRaccolta { get; }

        void AggiungiPuntoDiRaccolta(string nome, string indirizzo);
        void AggiungiPuntoDiRaccolta(PuntoDiRaccolta p);
    }
}
