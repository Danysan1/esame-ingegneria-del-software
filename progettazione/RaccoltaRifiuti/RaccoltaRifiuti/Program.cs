﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using RaccoltaRifiuti.Persistenza.XML;
using RaccoltaRifiuti.Presenter.WindowsForms;
using RaccoltaRifiuti.View.WindowsForms;
using RaccoltaRifiuti.Persistenza;
using RaccoltaRifiuti.Model;

namespace RaccoltaRifiuti
{
    static class Program
    {
        private static readonly string fileData = "data.xml", fileCredenziali = "credenziali.xml", admin = "admin", titolo = "Raccolta Rifiuti";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IPersistenza p = null;
            if (File.Exists(fileData))
                p = new PersistenzaXML(fileData, true);
            else
            {
                if (MessageBox.Show("Il file dati non esiste. Vuoi crearlo vuoto?", titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                {
                    File.Create(fileData).Close();
                    p = new PersistenzaXML(fileData, false);
                    p.SalvaPersona(new Persona(admin, admin, admin, true));
                }
            }

            ICredenziali c = null;
            if (File.Exists(fileCredenziali))
                c = new CredenzialiXML(fileCredenziali, true);
            else
            {
                if (MessageBox.Show("Il file credenziali non esiste. Vuoi crearlo vuoto?", titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                {
                    File.Create(fileCredenziali).Close();
                    c = new CredenzialiXML(fileCredenziali, false);
                    c.ModificaCredenziali(admin, admin);
                    MessageBox.Show("Accedi con codice " + admin + " e password " + admin, titolo, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            if (p != null && c != null)
            {
                try
                {
                    LoginForm lf = new LoginForm();
                    MainForm mf = new MainForm();
                    new MainPresenter(lf, mf, p, c);

                    Application.Run();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), titolo, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
