﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RaccoltaRifiuti.Utils
{
    public static class XmlDocumentExtension
    {
        public static XmlElement OttieniInsieme(this XmlDocument doc, string nome)
        {
            XmlElement model = (XmlElement)doc.SelectSingleNode("/Model"),
                insieme = (XmlElement)doc.SelectSingleNode("/Model/" + nome);

            if (model == null) // L'elemento model (root del documento) non esiste ancora
            {
                model = doc.CreateElement("Model");
                doc.AppendChild(model);
            }

            if (insieme == null) // L'elemento ricercato non esiste ancora
            {
                insieme = doc.CreateElement(nome);
                model.AppendChild(insieme);
            }

            return insieme;
        }
    }
}
