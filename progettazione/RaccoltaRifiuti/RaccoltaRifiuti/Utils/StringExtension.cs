﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Utils
{
    public static class StringExtension
    {
        public static string Ripulisci(this string s) {
            return s.ToUpper().Replace(" ", "");
        }
    }
}
