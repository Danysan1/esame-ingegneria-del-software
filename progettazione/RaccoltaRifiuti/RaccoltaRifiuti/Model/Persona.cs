﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using RaccoltaRifiuti.Utils;

namespace RaccoltaRifiuti.Model
{
    public class Persona : INotifyPropertyChanged
    {
        public string Nome { get; private set; }
        public string Cognome { get; private set; }
        public string CodiceFiscale { get; private set; } // primary key
        private bool _isAmministratore;
        public bool IsAmministratore
        {
            get { return _isAmministratore; }
            set
            {
                if (value != IsAmministratore)
                {
                    _isAmministratore = value;
                    if (PropertyChanged != null)
                        PropertyChanged.Invoke(this, new PropertyChangedEventArgs("IsAmministratore"));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Persona(string nome, string cognome, string codiceFiscale, bool isAmministratore = false)
        {
            if (String.IsNullOrWhiteSpace(nome))
                throw new ArgumentException("nome");
            if (String.IsNullOrWhiteSpace(cognome))
                throw new ArgumentException("cognome");
            if (String.IsNullOrWhiteSpace(codiceFiscale))
                throw new ArgumentException("codice fiscale");

            Nome = nome;
            Cognome = cognome;
            CodiceFiscale = codiceFiscale.Ripulisci();
            IsAmministratore = isAmministratore;
        }



        public override string ToString()
        {
            return Nome + " " + Cognome + " (" + CodiceFiscale + ")";
        }
    }

}
