﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Model
{
    public class Destinazione
    {
        public string Nome { get; private set; }
        public string Indirizzo { get; private set; } //primary key

        public Destinazione(string Nome, string Indirizzo)
        {
            if (null == Nome)
                throw new ArgumentNullException("nome");
            if (null == Indirizzo)
                throw new ArgumentNullException("indirizzo");
            if (String.IsNullOrWhiteSpace(Nome))
                throw new ArgumentException("nome");
            if (String.IsNullOrWhiteSpace(Indirizzo))
                throw new ArgumentException("indirizzo");

            this.Nome = Nome;
            this.Indirizzo = Indirizzo;
        }

        public override string ToString()
        {
            return Nome + " (" + Indirizzo + ")";
        }
    }
}
