﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaccoltaRifiuti.Model.Turni
{
    public class TurnoSettimanale : TurnoIntervalloDateOrarioFisso
    {
        private DateTime _ultimoInizio, _ultimaFine;

        private static readonly TimeSpan _unaSettimana = TimeSpan.FromDays(7);

        public TurnoSettimanale(TimeSpan oraInizio, DateTime dataInizio, DateTime dataFine, int id, Percorso percorso, ICollection<Persona> equipaggio, Camion camion, TipoRifiuto tipo)
            : base(oraInizio, dataInizio, dataFine, id, percorso, equipaggio, camion, tipo)
        {
            if (percorso.DurataPrevista >= _unaSettimana)
                throw new ArgumentException("La durata prevista del Percorso è troppo lunga per un turno settimanale");

            try
            {
                DateTime limite = DataDiFine + OrarioDiInizio;
                for (_ultimoInizio = PrimoInizio; _ultimoInizio + _unaSettimana < limite; _ultimoInizio += _unaSettimana)
                    ;
                _ultimaFine = _ultimoInizio + PercorsoEffuttuato.DurataPrevista;
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new ArgumentException("Hai inserito una data di fine troppo alta");
            }
        }

        public override bool InCorso(DateTime momento)
        {
            if (momento >= _ultimaFine || momento < PrimoInizio) // Fuori range
                return false;

            DateTime inizio;
            for (inizio = PrimoInizio; inizio + _unaSettimana < momento; inizio += _unaSettimana)
                ;
            DateTime fine = inizio + PercorsoEffuttuato.DurataPrevista;
            if (momento >= inizio && momento < fine) // E' in corso il turno odierno
                return true;

            DateTime inizoPrecedente = inizio - _unaSettimana,
                finePrecedente = fine - _unaSettimana;
            return momento >= inizoPrecedente && momento < finePrecedente; // E' in corso il viaggio iniziato la settimana scorsa
        }

        public override bool ProssimoInizio(DateTime momento, out DateTime prossimo)
        {
            if (momento < PrimoInizio) // Fuori dal range
            {
                prossimo = PrimoInizio;
                return true;
            }

            if (momento >= _ultimoInizio) // Fuori dal range
            {
                prossimo = DateTime.MinValue;
                return false;
            }

            for (prossimo = PrimoInizio; prossimo < momento; prossimo += _unaSettimana)
                ;
            return true;
        }

        public override bool ProssimaFine(DateTime momento, out DateTime prossimo)
        {
            if (momento < PrimaFine) // Fuori dal range
            {
                prossimo = PrimaFine;
                return true;
            }

            if (momento >= _ultimaFine) // Fuori dal range
            {
                prossimo = DateTime.MaxValue;
                return false;
            }

            for (prossimo = PrimaFine; prossimo < momento; prossimo += _unaSettimana)
                ;
            return true;
        }

        public override string ToString()
        {
            return base.ToString() + "; TurnoSettimanale";
        }
    }
}
