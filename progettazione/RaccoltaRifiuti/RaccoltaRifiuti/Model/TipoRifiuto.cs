﻿using System;

namespace RaccoltaRifiuti.Model
{
    public class TipoRifiuto
    {
        public string Nome { get; private set; } // primary key

        public TipoRifiuto(String nome)
        {
            if (String.IsNullOrWhiteSpace(nome))
                throw new ArgumentException("Nome invalido");

            Nome = nome;
        }

        public override string ToString()
        {
            return Nome;
        }
    }
}