﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Utils;

namespace RaccoltaRifiuti.Model
{
    public class Camion
    {
        public string Targa { get; private set; } // primary key
        public ICollection<TipoRifiuto> TipiSupportati { get; private set; }

        public Camion(string targa, ICollection<TipoRifiuto> tipiSupportati)
        {
            if (string.IsNullOrWhiteSpace(targa))
                throw new ArgumentException("targa nulla o vuota");

            if (tipiSupportati == null)
                throw new ArgumentNullException("tipiSupportati nullo");
            if(tipiSupportati.Count() == 0)
                throw new ArgumentException("tipiSupportati vuoto");
            if (tipiSupportati.Contains(null))
                throw new ArgumentException("tipiSupportati contiene null");

            Targa = targa.Ripulisci();
            TipiSupportati = tipiSupportati;
        }

        public override string ToString()
        {
            return Targa;
        }
    }
}
