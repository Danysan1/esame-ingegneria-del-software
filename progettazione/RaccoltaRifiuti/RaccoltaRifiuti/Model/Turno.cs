﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace RaccoltaRifiuti.Model
{
    public abstract class Turno
    {
        public int ID { get; private set; } // primary key
        public Percorso PercorsoEffuttuato { get; private set; }
        public ICollection<Persona> Equipaggio { get; private set; }
        public Camion CamionUsato { get; private set; }
        public TipoRifiuto Tipo { get; private set; }

        public abstract bool InCorso(DateTime momento);
        public abstract bool ProssimoInizio(DateTime momento, out DateTime prossimo);
        public abstract bool ProssimaFine(DateTime momento, out DateTime prossimo);

        protected Turno(int id, Percorso Percorso, ICollection<Persona> equipaggio, Camion camion, TipoRifiuto tipo)
        {
            ID = id;

            if (Percorso == null)
                throw new ArgumentNullException("Percorso");
            PercorsoEffuttuato = Percorso;

            if (equipaggio == null)
                throw new ArgumentNullException("equipaggio nullo");
            if (equipaggio.Count() == 0)
                throw new ArgumentException("equipaggio vuoto");
            if (equipaggio.Contains(null))
                throw new ArgumentException("equipaggio contiene null");
            Equipaggio = equipaggio;

            if (camion == null)
                throw new ArgumentNullException("camion");
            CamionUsato = camion;

            if (tipo == null)
                throw new ArgumentNullException("tipo");
            if (!camion.TipiSupportati.Contains(tipo))
                throw new ApplicationException("tipo non supportato dal camion");
            Tipo = tipo;
        }

        public override string ToString()
        {
            return "Turno: " + ID + " / " + PercorsoEffuttuato + " / " + CamionUsato + " / " + Tipo + " / {" + string.Join(", ", from p in Equipaggio select p.CodiceFiscale) + "}";
        }
    }
}
