﻿using NUnit.Framework;
using System;
using RaccoltaRifiuti.Model;

namespace UnitTest
{
    [TestFixture]
    public class TestPuntoDiRaccolta
    {
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void NomeNullo()
        {
            new PuntoDiRaccolta(null, "Via bho");
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void IndirizzoNullo()
        {
            new PuntoDiRaccolta("qwerty", null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void NomeVuoto()
        {
            new PuntoDiRaccolta(" ", "Via bho");
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void IndirizzoVuoto()
        {
            new PuntoDiRaccolta("qwerty", " ");
        }
    }
}