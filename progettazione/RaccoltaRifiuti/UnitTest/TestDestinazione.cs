﻿using NUnit.Framework;
using System;
using RaccoltaRifiuti.Model;

namespace UnitTest
{
    [TestFixture]
    class TestDestinazione
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NomeNullo()
        {
            new Destinazione(null, "Via Pediano 52");
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IndirizzoNullo()
        {
            new Destinazione("Discarica Tre Monti", null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void NomeVuoto()
        {
            new Destinazione(" ", "Via Pediano 52");
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void IndirizzoVuoto()
        {
            new Destinazione("Discarica Tre Monti", " ");
        }
    }
}
