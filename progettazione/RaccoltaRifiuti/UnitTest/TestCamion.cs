﻿using NUnit.Framework;
using System;
using RaccoltaRifiuti.Model;
using System.Collections.Generic;

namespace UnitTest
{
    [TestFixture]
    public class TestCamion
    {
        private List<TipoRifiuto> _esempioListaTipoRifiuto;

        [SetUp]
        public void SetUp() {
            _esempioListaTipoRifiuto = new List<TipoRifiuto>();
            _esempioListaTipoRifiuto.Add(new TipoRifiuto("umido"));
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void TargaNulla()
        {
            new Camion(null, _esempioListaTipoRifiuto);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void TargaVuota()
        {
            new Camion(" ", _esempioListaTipoRifiuto);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TipiSupportatiNullo()
        {
            new Camion("ab123cd", null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void TipiSupportatiVuoto()
        {
            List<TipoRifiuto> listaVuota = new List<TipoRifiuto>();
            new Camion("ab123cd", listaVuota);
        }
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void TipiSupportatiContieneNull()
        {
            _esempioListaTipoRifiuto.Add(null);
            new Camion("ab123cd", _esempioListaTipoRifiuto);
        }
    }
}