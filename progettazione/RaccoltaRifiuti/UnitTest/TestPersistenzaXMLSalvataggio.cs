﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Persistenza.XML;
using RaccoltaRifiuti.Model;
using NUnit.Framework;
using System.IO;
using RaccoltaRifiuti.Persistenza;

namespace UnitTest
{
    [TestFixture]
    class TestPersistenzaXMLSalvataggio // Test su PersistenzaXML che usano "nuovo.xml" e non richiedono particolari setup
    {
        private IPersistenza _persistenza;

        [SetUp]
        public void Setup()
        {
            _persistenza = new PersistenzaXML("nuovo.xml", false);
        }

        [Test]
        public void SalvaDestinazioneNull()
        {
            Assert.AreEqual(false, _persistenza.SalvaDestinazione(null));
        }

        [Test]
        public void SalvaPersonaNull()
        {
            Assert.AreEqual(false, _persistenza.SalvaPersona(null));
        }

        [Test]
        public void SalvaPuntoDiRaccoltaNull()
        {
            Assert.AreEqual(false, _persistenza.SalvaPuntoDiRaccolta(null));
        }

        [Test]
        public void SalvaTipoDiRifiutoNull()
        {
            Assert.AreEqual(false, _persistenza.SalvaTipoDiRifiuto(null));
        }

        [Test]
        public void SalvaCamionNull()
        {
            Assert.AreEqual(false, _persistenza.SalvaCamion(null));
        }

        [Test]
        public void SalvaTurnoNull()
        {
            Assert.AreEqual(false, _persistenza.SalvaTurno(null));
        }

        [Test]
        public void SalvaPercorsoNull()
        {
            Assert.AreEqual(false, _persistenza.SalvaPercorso(null));
        }

        [Test]
        public void SalvaDestinazione()
        {
            Assert.AreEqual(true, _persistenza.SalvaDestinazione(new Destinazione("pippo", "pluto")));
            Assert.AreEqual(1, _persistenza.CaricaDestinazioni().Count());
        }

        [Test]
        public void SalvaPersona()
        {
            Assert.AreEqual(true, _persistenza.SalvaPersona(new Persona("pippo", "pluto", "paperino", true)));
            Assert.AreEqual(1, _persistenza.CaricaPersone().Count());
        }

        [Test]
        public void SalvaPuntoDiRaccolta()
        {
            Assert.AreEqual(true, _persistenza.SalvaPuntoDiRaccolta(new PuntoDiRaccolta("pippo", "pluto")));
            Assert.AreEqual(1, _persistenza.CaricaPuntiDiRaccolta().Count());
        }

        [Test]
        public void SalvaTipoDiRifiuto()
        {
            Assert.AreEqual(true, _persistenza.SalvaTipoDiRifiuto(new TipoRifiuto("vetro")));
            Assert.AreEqual(1, _persistenza.CaricaTipiDiRifiuto().Count());
        }
    }
}
