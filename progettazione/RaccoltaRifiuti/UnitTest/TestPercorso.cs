﻿using NUnit.Framework;
using System;
using RaccoltaRifiuti.Model;
using System.Collections.Generic;

namespace UnitTest
{
    [TestFixture]
    public class TestPercorso
    {
        Destinazione _esempioDestinatione;
        List<PuntoDiRaccolta> _esempioItinerario;

        [SetUp]
        public void Setup()
        {
            _esempioDestinatione = new Destinazione("a", "a");
            _esempioItinerario = new List<PuntoDiRaccolta>();
            _esempioItinerario.Add(new PuntoDiRaccolta("a", "a"));
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void DurataNegativa()
        {
            new Percorso(TimeSpan.MinValue, 123, _esempioDestinatione, _esempioItinerario);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void DurataMinoreZero()
        {
            new Percorso(TimeSpan.MinValue, 123, _esempioDestinatione, _esempioItinerario);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void DurataZero()
        {
            new Percorso(TimeSpan.Zero, 123, _esempioDestinatione, _esempioItinerario);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DestinazioneNulla()
        {
            new Percorso(TimeSpan.MaxValue, 123, null, _esempioItinerario);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ItinerarioNullo()
        {
            new Percorso(TimeSpan.MaxValue, 123, _esempioDestinatione, null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void ItinerarioVuoto()
        {
            new Percorso(TimeSpan.MaxValue, 123, _esempioDestinatione, new List<PuntoDiRaccolta>());
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void ItinerarioContieneNull()
        {
            _esempioItinerario.Add(null);
            new Percorso(TimeSpan.MaxValue, 123, _esempioDestinatione, _esempioItinerario);
        }
    }
}