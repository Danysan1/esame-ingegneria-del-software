﻿using NUnit.Framework;
using System;
using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.Model.Turni;
using System.Collections.Generic;

namespace UnitTest
{
    [TestFixture]
    public class TestTurno
    {
        private TipoRifiuto _esempioTipoRifiuto;
        private Percorso _esempioPercorso;
        private Camion _esempioCamion;
        private List<Persona> _esempioListaPersone;
        private List<PuntoDiRaccolta> _esempioListaPunti;
        private DateTime _esempioOraInizio, _esempioDataInizio, _esempioDataFine, _inizioPrimoTurno, _finePrimoTurno, _inizioSecondoTurno, _fineSecondoTurno, _inizioTerzoTurno, _fineTerzoTurno, _inizioUltimoTurno, _fineUltimoTurno;
        private TimeSpan _unGiorno, _unSecondo;

        [SetUp]
        public void SetUp()
        {
            _esempioTipoRifiuto = new TipoRifiuto("umido");
            List<TipoRifiuto> tipi = new List<TipoRifiuto>();
            tipi.Add(_esempioTipoRifiuto);

            _esempioListaPunti = new List<PuntoDiRaccolta>();
            _esempioListaPunti.Add(new PuntoDiRaccolta("pippo", "pluto"));
            _esempioPercorso = new Percorso(new TimeSpan(2, 0, 0), 1, new Destinazione("paperino", "minnie"), _esempioListaPunti);

            _esempioListaPersone = new List<Persona>();
            _esempioListaPersone.Add(new Persona("pippo", "pluto", "paperino", true));

            _esempioCamion = new Camion("xy999wz", tipi);

            _unSecondo = new TimeSpan(0, 0, 1);
            _unGiorno = new TimeSpan(1, 0, 0, 0);

            _esempioOraInizio = new DateTime(1, 1, 1, 23, 0, 0);
            _esempioDataInizio = new DateTime(2007, 7, 7, 0, 0, 0);
            _esempioDataFine = new DateTime(2008, 8, 8, 0, 0, 0);
            _inizioPrimoTurno = _esempioDataInizio + _esempioOraInizio.TimeOfDay;
            _finePrimoTurno = _inizioPrimoTurno + _esempioPercorso.DurataPrevista;
            _inizioSecondoTurno = _inizioPrimoTurno + _unGiorno;
            _fineSecondoTurno = _finePrimoTurno + _unGiorno;
            _inizioTerzoTurno = _inizioSecondoTurno + _unGiorno;
            _fineTerzoTurno = _fineSecondoTurno + _unGiorno;
            _inizioUltimoTurno = _esempioDataFine + _esempioOraInizio.TimeOfDay;
            _fineUltimoTurno = _inizioUltimoTurno + _esempioPercorso.DurataPrevista;
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PercorsoNullo()
        {
            new TurnoGiornaliero(_esempioOraInizio, _esempioDataInizio, _esempioDataFine, 123, null, _esempioListaPersone, _esempioCamion, _esempioTipoRifiuto);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ListaPersoneNulla()
        {
            new TurnoGiornaliero(_esempioOraInizio, _esempioDataInizio, _esempioDataFine, 123, _esempioPercorso, null, _esempioCamion, _esempioTipoRifiuto);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void ListaPersoneVuota()
        {
            new TurnoGiornaliero(_esempioOraInizio, _esempioDataInizio, _esempioDataFine, 123, _esempioPercorso, new List<Persona>(), _esempioCamion, _esempioTipoRifiuto);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void ListaPersoneContieneNull()
        {
            _esempioListaPersone.Add(null);
            new TurnoGiornaliero(_esempioOraInizio, _esempioDataInizio, _esempioDataFine, 123, _esempioPercorso, _esempioListaPersone, _esempioCamion, _esempioTipoRifiuto);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CamionNullo()
        {
            new TurnoGiornaliero(_esempioOraInizio, _esempioDataInizio, _esempioDataFine, 123, _esempioPercorso, _esempioListaPersone, null, _esempioTipoRifiuto);
        }

        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TipoRifutoNullo()
        {
            new TurnoGiornaliero(_esempioOraInizio, _esempioDataInizio, _esempioDataFine, 123, _esempioPercorso, _esempioListaPersone, _esempioCamion, null);
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void TipoRifutoNonSupportato()
        {
            new TurnoGiornaliero(_esempioOraInizio, _esempioDataInizio, _esempioDataFine, 123, _esempioPercorso, _esempioListaPersone, _esempioCamion, new TipoRifiuto("vetro"));
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void OrarioTroppoAlto()
        {
            new TurnoSettimanale(new TimeSpan(25,0,0), _esempioDataInizio, _esempioDataFine, 123, _esempioPercorso, _esempioListaPersone, _esempioCamion, new TipoRifiuto("vetro"));
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void DataFinePrecedeInizio()
        {
            new TurnoGiornaliero(_esempioOraInizio, _esempioDataFine, _esempioDataInizio, 123, _esempioPercorso, _esempioListaPersone, _esempioCamion, _esempioTipoRifiuto);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void UltimaFineSforaMassimoDateTime()
        {
            new TurnoGiornaliero(_esempioOraInizio, _esempioDataInizio, DateTime.MaxValue, 123, _esempioPercorso, _esempioListaPersone, _esempioCamion, _esempioTipoRifiuto);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void DurataPrevistaGiornalieroTroppoLunga()
        {
            new TurnoGiornaliero(_esempioOraInizio, _esempioDataInizio, _esempioDataFine, 123, new Percorso(new TimeSpan(25, 0, 0), 1, new Destinazione("paperino", "minnie"), _esempioListaPunti), _esempioListaPersone, _esempioCamion, _esempioTipoRifiuto);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void DurataPrevistaSettimanaleTroppoLunga()
        {
            new TurnoSettimanale(_esempioOraInizio.TimeOfDay, _esempioDataInizio, _esempioDataFine, 123, new Percorso(new TimeSpan(8, 0, 0, 0), 1, new Destinazione("paperino", "minnie"), _esempioListaPunti), _esempioListaPersone, _esempioCamion, _esempioTipoRifiuto);
        }

        [Test]
        public void InCorso()
        {
            Turno t = new TurnoGiornaliero(_esempioOraInizio, _esempioDataInizio, _esempioDataFine, 123, _esempioPercorso, _esempioListaPersone, _esempioCamion, _esempioTipoRifiuto);

            Assert.AreEqual(false, t.InCorso(DateTime.MinValue));

            // Primo turno
            Assert.AreEqual(false, t.InCorso(_inizioPrimoTurno - _unSecondo));
            Assert.AreEqual(true, t.InCorso(_inizioPrimoTurno));
            Assert.AreEqual(true, t.InCorso(_inizioPrimoTurno + _unSecondo));
            Assert.AreEqual(true, t.InCorso(_finePrimoTurno - _unSecondo));
            Assert.AreEqual(false, t.InCorso(_finePrimoTurno));
            Assert.AreEqual(false, t.InCorso(_finePrimoTurno + _unSecondo));

            // Secondo turno
            Assert.AreEqual(false, t.InCorso(_inizioSecondoTurno - _unSecondo));
            Assert.AreEqual(true, t.InCorso(_inizioSecondoTurno));
            Assert.AreEqual(true, t.InCorso(_inizioSecondoTurno + _unSecondo));
            Assert.AreEqual(true, t.InCorso(_fineSecondoTurno - _unSecondo));
            Assert.AreEqual(false, t.InCorso(_fineSecondoTurno));
            Assert.AreEqual(false, t.InCorso(_fineSecondoTurno + _unSecondo));

            //Ultimo turno
            Assert.AreEqual(false, t.InCorso(_inizioUltimoTurno - _unSecondo));
            Assert.AreEqual(true, t.InCorso(_inizioUltimoTurno));
            Assert.AreEqual(true, t.InCorso(_inizioUltimoTurno + _unSecondo));
            Assert.AreEqual(true, t.InCorso(_fineUltimoTurno - _unSecondo));
            Assert.AreEqual(false, t.InCorso(_fineUltimoTurno));
            Assert.AreEqual(false, t.InCorso(_fineUltimoTurno + _unSecondo));

            Assert.AreEqual(false, t.InCorso(DateTime.MaxValue));
        }

        [Test]
        public void ProssimoInizio()
        {
            Turno t = new TurnoGiornaliero(_esempioOraInizio, _esempioDataInizio, _esempioDataFine, 123, _esempioPercorso, _esempioListaPersone, _esempioCamion, _esempioTipoRifiuto);
            DateTime prossimo;

            Assert.AreEqual(true, t.ProssimoInizio(DateTime.MinValue, out prossimo));
            Assert.AreEqual(prossimo, _inizioPrimoTurno);

            // Primo turno
            Assert.AreEqual(true, t.ProssimoInizio(_inizioPrimoTurno - _unSecondo, out prossimo));
            Assert.AreEqual(prossimo, _inizioPrimoTurno);
            Assert.AreEqual(true, t.ProssimoInizio(_inizioPrimoTurno, out prossimo));
            Assert.AreEqual(prossimo, _inizioSecondoTurno);
            Assert.AreEqual(true, t.ProssimoInizio(_inizioPrimoTurno + _unSecondo, out prossimo));
            Assert.AreEqual(prossimo, _inizioSecondoTurno);
            Assert.AreEqual(true, t.ProssimoInizio(_finePrimoTurno, out prossimo));
            Assert.AreEqual(prossimo, _inizioSecondoTurno);

            // Secondo turno
            Assert.AreEqual(true, t.ProssimoInizio(_inizioSecondoTurno - _unSecondo, out prossimo));
            Assert.AreEqual(prossimo, _inizioSecondoTurno);
            Assert.AreEqual(true, t.ProssimoInizio(_inizioSecondoTurno, out prossimo));
            Assert.AreEqual(prossimo, _inizioTerzoTurno);
            Assert.AreEqual(true, t.ProssimoInizio(_inizioSecondoTurno + _unSecondo, out prossimo));
            Assert.AreEqual(prossimo, _inizioTerzoTurno);
            Assert.AreEqual(true, t.ProssimoInizio(_fineSecondoTurno, out prossimo));
            Assert.AreEqual(prossimo, _inizioTerzoTurno);

            // Ultimo TurnoGiornaliero
            Assert.AreEqual(true, t.ProssimoInizio(_inizioUltimoTurno - _unSecondo, out prossimo));
            Assert.AreEqual(prossimo, _inizioUltimoTurno);
            Assert.AreEqual(false, t.ProssimoInizio(_inizioUltimoTurno, out prossimo));
            Assert.AreEqual(false, t.ProssimoInizio(_inizioUltimoTurno + _unSecondo, out prossimo));
            Assert.AreEqual(false, t.ProssimoInizio(_fineUltimoTurno, out prossimo));

            Assert.AreEqual(false, t.ProssimoInizio(DateTime.MaxValue, out prossimo));
        }

        [Test]
        public void ProssimaFine()
        {
            Turno t = new TurnoGiornaliero(_esempioOraInizio, _esempioDataInizio, _esempioDataFine, 123, _esempioPercorso, _esempioListaPersone, _esempioCamion, _esempioTipoRifiuto);
            DateTime prossimo;

            Assert.AreEqual(true, t.ProssimaFine(DateTime.MinValue, out prossimo));
            Assert.AreEqual(prossimo, _finePrimoTurno);

            // Primo turno
            Assert.AreEqual(true, t.ProssimaFine(_inizioPrimoTurno, out prossimo));
            Assert.AreEqual(prossimo, _finePrimoTurno);
            Assert.AreEqual(true, t.ProssimaFine(_finePrimoTurno - _unSecondo, out prossimo));
            Assert.AreEqual(prossimo, _finePrimoTurno);
            Assert.AreEqual(true, t.ProssimaFine(_finePrimoTurno, out prossimo));
            Assert.AreEqual(prossimo, _fineSecondoTurno);
            Assert.AreEqual(true, t.ProssimaFine(_finePrimoTurno + _unSecondo, out prossimo));
            Assert.AreEqual(prossimo, _fineSecondoTurno);

            // Secondo turno
            Assert.AreEqual(true, t.ProssimaFine(_inizioSecondoTurno, out prossimo));
            Assert.AreEqual(prossimo, _fineSecondoTurno);
            Assert.AreEqual(true, t.ProssimaFine(_fineSecondoTurno - _unSecondo, out prossimo));
            Assert.AreEqual(prossimo, _fineSecondoTurno);
            Assert.AreEqual(true, t.ProssimaFine(_fineSecondoTurno, out prossimo));
            Assert.AreEqual(prossimo, _fineTerzoTurno);
            Assert.AreEqual(true, t.ProssimaFine(_fineSecondoTurno + _unSecondo, out prossimo));
            Assert.AreEqual(prossimo, _fineTerzoTurno);

            // Ultimo turno
            Assert.AreEqual(true, t.ProssimaFine(_inizioUltimoTurno, out prossimo));
            Assert.AreEqual(prossimo, _fineUltimoTurno);
            Assert.AreEqual(true, t.ProssimaFine(_fineUltimoTurno - _unSecondo, out prossimo));
            Assert.AreEqual(prossimo, _fineUltimoTurno);
            Assert.AreEqual(false, t.ProssimaFine(_fineUltimoTurno, out prossimo));
            Assert.AreEqual(false, t.ProssimaFine(_fineUltimoTurno + _unSecondo, out prossimo));

            Assert.AreEqual(false, t.ProssimaFine(DateTime.MaxValue, out prossimo));
        }
    }
}