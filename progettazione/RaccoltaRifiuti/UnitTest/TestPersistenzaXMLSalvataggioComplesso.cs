﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Persistenza.XML;
using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.Model.Turni;
using NUnit.Framework;
using System.IO;
using RaccoltaRifiuti.Persistenza;

namespace UnitTest
{
    [TestFixture]
    class TestPersistenzaXMLSalvataggioComplesso // Test su PersistenzaXML che usano "nuovo.xml" e richiedono un setup avanzato
    {
        private IPersistenza _persistenza;
        private TipoRifiuto _esempioTipoRifiuto;
        private List<TipoRifiuto> _esempioListaTipoRifiuto;
        private Percorso _esempioPercorso;
        private List<Percorso> _esempioListaPercorso;
        private Persona _esempioPersona;
        private List<Persona> _esempioListaPersone;
        private Camion _esempioCamion;
        private List<Camion> _esempioListaCamion;
        private DateTime _esempioOra1, _esempioData1, _esempioData2;
        private Destinazione _esempioDestinazione;
        private List<Destinazione> _esempioListaDestinazioni;
        private PuntoDiRaccolta _esempioPuntoDiRaccolta;
        private List<PuntoDiRaccolta> _esempioListaPuntoDiRaccolta;
        private Turno _esempioTurno;
        private List<Turno> _esempioListaTurno;

        [SetUp]
        public void Setup()
        {
            _persistenza = new PersistenzaXML("nuovo.xml", false);

            _esempioTipoRifiuto = new TipoRifiuto("umido");
            _esempioListaTipoRifiuto = new List<TipoRifiuto>();
            _esempioListaTipoRifiuto.Add(_esempioTipoRifiuto);

            _esempioDestinazione = new Destinazione("pippo", "pluto");
            _esempioListaDestinazioni = new List<Destinazione>();
            _esempioListaDestinazioni.Add(_esempioDestinazione);

            _esempioPuntoDiRaccolta = new PuntoDiRaccolta("pippo", "pluto");
            _esempioListaPuntoDiRaccolta = new List<PuntoDiRaccolta>();
            _esempioListaPuntoDiRaccolta.Add(_esempioPuntoDiRaccolta);

            _esempioPercorso = new Percorso(new TimeSpan(1, 0, 0), 12, _esempioDestinazione, _esempioListaPuntoDiRaccolta);
            _esempioListaPercorso = new List<Percorso>();
            _esempioListaPercorso.Add(_esempioPercorso);

            _esempioPersona = new Persona("pippo", "pluto", "paperino", true);
            _esempioListaPersone = new List<Persona>();
            _esempioListaPersone.Add(_esempioPersona);

            _esempioCamion = new Camion("ab123cd", _esempioListaTipoRifiuto);
            _esempioListaCamion = new List<Camion>();
            _esempioListaCamion.Add(_esempioCamion);

            _esempioOra1 = new DateTime(1, 1, 1, 12, 0, 0);
            _esempioData1 = new DateTime(2015, 1, 12, 0, 0, 0);
            _esempioData2 = new DateTime(2015, 12, 1, 0, 0, 0);

            _esempioTurno = new TurnoGiornaliero(_esempioOra1, _esempioData1, _esempioData2, 123, _esempioPercorso, _esempioListaPersone, _esempioCamion, _esempioTipoRifiuto);
            _esempioListaTurno = new List<Turno>();
            _esempioListaTurno.Add(_esempioTurno);
        }

        [Test]
        public void SalvaCamion()
        {
            Assert.AreEqual(true, _persistenza.SalvaCamion(_esempioCamion));
            Assert.AreEqual(1, _persistenza.CaricaCamion(_esempioListaTipoRifiuto).Count());
        }

        [Test]
        public void SalvaTurno()
        {
            Assert.AreEqual(true, _persistenza.SalvaTurno(_esempioTurno));
            Assert.AreEqual(1, _persistenza.CaricaTurni(_esempioListaPersone, _esempioListaTipoRifiuto, _esempioListaCamion, _esempioListaPercorso).Count());
        }

        [Test]
        public void SalvaPercorso()
        {
            Assert.AreEqual(true, _persistenza.SalvaPercorso(_esempioPercorso));
            Assert.AreEqual(1, _persistenza.CaricaPercorsi(_esempioListaDestinazioni, _esempioListaPuntoDiRaccolta).Count());
        }

        [Test]
        public void SalvaTutto()
        {
            Assert.AreEqual(true, _persistenza.SalvaTutto(_esempioListaDestinazioni, _esempioListaPersone, _esempioListaPuntoDiRaccolta, _esempioListaTipoRifiuto, _esempioListaCamion, _esempioListaPercorso, _esempioListaTurno));

            IEnumerable<Persona> persone = _persistenza.CaricaPersone();
            Assert.AreEqual(_esempioListaPersone.Count, persone.Count());
            IEnumerable<TipoRifiuto> tipi = _persistenza.CaricaTipiDiRifiuto().ToList();
            Assert.AreEqual(_esempioListaTipoRifiuto.Count, tipi.Count());
            IEnumerable<PuntoDiRaccolta> punti = _persistenza.CaricaPuntiDiRaccolta();
            Assert.AreEqual(_esempioListaPuntoDiRaccolta.Count, punti.Count());
            IEnumerable<Destinazione> destinazioni = _persistenza.CaricaDestinazioni();
            Assert.AreEqual(_esempioListaDestinazioni.Count, destinazioni.Count());
            IEnumerable<Camion> camion = _persistenza.CaricaCamion(tipi);
            Assert.AreEqual(_esempioListaCamion.Count, camion.Count());
            Assert.AreEqual(_esempioCamion.TipiSupportati.Count(), camion.ElementAt(0).TipiSupportati.Count());
            IEnumerable<Percorso> Percorsi = _persistenza.CaricaPercorsi(destinazioni, punti);
            Assert.AreEqual(_esempioListaPercorso.Count, Percorsi.Count());
            Assert.AreEqual(_esempioPercorso.Itinerario.Count(), Percorsi.ElementAt(0).Itinerario.Count());
            IEnumerable<Turno> turni = _persistenza.CaricaTurni(persone, tipi, camion, Percorsi);
            Assert.AreEqual(_esempioListaTurno.Count, turni.Count());
            Assert.AreEqual(_esempioTurno.Equipaggio.Count(), turni.ElementAt(0).Equipaggio.Count());
        }

        [Test]
        public void SalvataggioCompleto()
        {
            Assert.AreEqual(true, _persistenza.SalvaPersona(_esempioPersona));
            Assert.AreEqual(true, _persistenza.SalvaTipoDiRifiuto(_esempioTipoRifiuto));
            Assert.AreEqual(true, _persistenza.SalvaPuntoDiRaccolta(_esempioPuntoDiRaccolta));
            Assert.AreEqual(true, _persistenza.SalvaDestinazione(_esempioDestinazione));
            Assert.AreEqual(true, _persistenza.SalvaCamion(_esempioCamion));
            Assert.AreEqual(true, _persistenza.SalvaPercorso(_esempioPercorso));
            Assert.AreEqual(true, _persistenza.SalvaTurno(_esempioTurno));

            IEnumerable<Persona> persone = _persistenza.CaricaPersone();
            Assert.AreEqual(1, persone.Count());
            IEnumerable<TipoRifiuto> tipi = _persistenza.CaricaTipiDiRifiuto().ToList();
            Assert.AreEqual(1, tipi.Count());
            IEnumerable<PuntoDiRaccolta> punti = _persistenza.CaricaPuntiDiRaccolta();
            Assert.AreEqual(1, punti.Count());
            IEnumerable<Destinazione> destinazioni = _persistenza.CaricaDestinazioni();
            Assert.AreEqual(1, destinazioni.Count());
            IEnumerable<Camion> camion = _persistenza.CaricaCamion(tipi);
            Assert.AreEqual(1, camion.Count());
            Assert.AreEqual(_esempioCamion.TipiSupportati.Count(), camion.ElementAt(0).TipiSupportati.Count());
            IEnumerable<Percorso> Percorsi = _persistenza.CaricaPercorsi(destinazioni, punti);
            Assert.AreEqual(1, Percorsi.Count());
            Assert.AreEqual(_esempioPercorso.Itinerario.Count(), Percorsi.ElementAt(0).Itinerario.Count());
            IEnumerable<Turno> turni = _persistenza.CaricaTurni(persone, tipi, camion, Percorsi);
            Assert.AreEqual(1, turni.Count());
            Assert.AreEqual(_esempioTurno.Equipaggio.Count(), turni.ElementAt(0).Equipaggio.Count());
        }

        [Test]
        public void InvalidFile()
        {
            PersistenzaXML persistenza = new PersistenzaXML("C:/Windows/explorer.exe", false);
            Assert.AreEqual(false, persistenza.SalvaPersona(_esempioPersona));
            Assert.AreEqual(false, persistenza.SalvaTipoDiRifiuto(_esempioTipoRifiuto));
            Assert.AreEqual(false, persistenza.SalvaPuntoDiRaccolta(_esempioPuntoDiRaccolta));
            Assert.AreEqual(false, persistenza.SalvaDestinazione(_esempioDestinazione));
            Assert.AreEqual(false, persistenza.SalvaCamion(_esempioCamion));
            Assert.AreEqual(false, persistenza.SalvaPercorso(_esempioPercorso));
            Assert.AreEqual(false, persistenza.SalvaTurno(_esempioTurno));
        }
    }
}
