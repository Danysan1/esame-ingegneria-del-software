﻿using NUnit.Framework;
using System;
using RaccoltaRifiuti.Model;

namespace UnitTest
{
    [TestFixture]
    public class TestTipoRifiuto
    {
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void NomeNullo()
        {
            new TipoRifiuto(null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void NomeVuoto()
        {
            new TipoRifiuto(" ");
        }
    }
}