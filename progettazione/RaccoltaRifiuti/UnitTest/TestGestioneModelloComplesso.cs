﻿using NUnit.Framework;
using RaccoltaRifiuti.Model;
using RaccoltaRifiuti.Model.Turni;
using RaccoltaRifiuti.Persistenza;
using RaccoltaRifiuti.Persistenza.XML;
using RaccoltaRifiuti.Presenter.Dati;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    class TestGestioneModelloComplesso
    {
        private IPersistenza _esempioPersistenza;
        private GestioneModello _gestione;
        private static readonly DateTime _inizio = new DateTime(2007, 7, 7), _fine = new DateTime(2008, 8, 8), _ventitre = new DateTime(1, 1, 1, 23, 0, 0);

        [SetUp]
        public void SetUp()
        {
            _esempioPersistenza = new PersistenzaXML("../../TestPersistenzaXMLCaricamento.xml", true);

            _gestione = new GestioneModello(new PersistenzaXML("pippo.xml", false));
            _gestione.AggiungiTipoDiRifiuto(new TipoRifiuto("umido"));
            _gestione.AggiungiCamion(new Camion("ab123cd", _gestione.TipiDiRifiuto));
            _gestione.AggiungiDestinazione(new Destinazione("discarica pippo", "via del pattume"));
            _gestione.AggiungiPuntoDiRaccolta(new PuntoDiRaccolta("cassonetto pluto", "via della pulizia"));
            _gestione.AggiungiPercorso(new Percorso(new TimeSpan(2, 0, 0), _gestione.NuovoIdPercorso(), _gestione.Destinazioni[0], _gestione.PuntiDiRaccolta));
            _gestione.AggiungiPersona(new Persona("abcd12345efgh", "Paperino", "De Paperinis"));
        }

        [Test]
        public void CaricamentoCorretto()
        {
            GestioneModello g = new GestioneModello(_esempioPersistenza);
            Assert.AreEqual(2, g.Persone.Count);
            Assert.AreEqual(1, g.Destinazioni.Count);
            Assert.AreEqual(1, g.PuntiDiRaccolta.Count);
            Assert.AreEqual(2, g.TipiDiRifiuto.Count);
            Assert.AreEqual(1, g.Camion.Count);
            Assert.AreEqual(1, g.Percorsi.Count);
            Assert.AreEqual(1, g.Turni.Count);
            Console.WriteLine(g.Turni[0]);
        }

        [Test]
        public void AggiungiDatiValidi()
        {
            Assert.AreEqual(1, _gestione.TipiDiRifiuto.Count);
            Assert.AreEqual(1, _gestione.Camion.Count);
            Assert.AreEqual(1, _gestione.Destinazioni.Count);
            Assert.AreEqual(1, _gestione.PuntiDiRaccolta.Count);
            Assert.AreEqual(1, _gestione.Percorsi.Count);
            Assert.AreEqual(1, _gestione.Persone.Count);
            _gestione.AggiungiTurno(new TurnoGiornaliero(_ventitre, _inizio, _fine, 1, _gestione.Percorsi[0], _gestione.Persone, _gestione.Camion[0], _gestione.TipiDiRifiuto[0]));
            _gestione.AggiungiTurno(new TurnoGiornaliero(new DateTime(1, 1, 1, 1, 0, 0), _inizio, _fine, 2, _gestione.Percorsi[0], _gestione.Persone, _gestione.Camion[0], _gestione.TipiDiRifiuto[0]));
            _gestione.AggiungiTurno(new TurnoGiornaliero(new DateTime(1, 1, 1, 3, 0, 0), _inizio, _fine, 3, _gestione.Percorsi[0], _gestione.Persone, _gestione.Camion[0], _gestione.TipiDiRifiuto[0]));
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void AggiungiCamionDuplicato()
        {
            _gestione.AggiungiCamion("xy123ij", _gestione.TipiDiRifiuto);
            _gestione.AggiungiCamion("xy123ij", _gestione.TipiDiRifiuto);
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void AggiungiTurnoDuplicato()
        {
            _gestione.AggiungiTurno(new TurnoGiornaliero(_ventitre, _inizio, _fine, 123, _gestione.Percorsi[0], _gestione.Persone, _gestione.Camion[0], _gestione.TipiDiRifiuto[0]));
            _gestione.AggiungiTurno(new TurnoGiornaliero(new DateTime(1, 1, 1, 5, 0, 0), _inizio, _fine, 123, _gestione.Percorsi[0], _gestione.Persone, _gestione.Camion[0], _gestione.TipiDiRifiuto[0]));
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void AggiungiTurnoSovrappostoAllInizio()
        {
            _gestione.AggiungiTurno(new TurnoGiornaliero(_ventitre, _inizio, _fine, 1, _gestione.Percorsi[0], _gestione.Persone, _gestione.Camion[0], _gestione.TipiDiRifiuto[0]));
            _gestione.AggiungiTurno(new TurnoGiornaliero(new DateTime(1, 1, 1, 0, 30, 0), _inizio, _fine, 2, _gestione.Percorsi[0], _gestione.Persone, _gestione.Camion[0], _gestione.TipiDiRifiuto[0]));
        }

        [Test]
        public void AggiungiTurnoConfinante()
        {
            _gestione.AggiungiTurno(new TurnoGiornaliero(_ventitre, _inizio, _fine, 1, _gestione.Percorsi[0], _gestione.Persone, _gestione.Camion[0], _gestione.TipiDiRifiuto[0]));
            _gestione.AggiungiTurno(new TurnoGiornaliero(new DateTime(1, 1, 1, 1, 0, 0), _inizio, _fine, 2, _gestione.Percorsi[0], _gestione.Persone, _gestione.Camion[0], _gestione.TipiDiRifiuto[0]));
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void AggiungiTurnoSovrappostoAllaFine()
        {
            _gestione.AggiungiTurno(new TurnoGiornaliero(_ventitre, _inizio, _fine, 1, _gestione.Percorsi[0], _gestione.Persone, _gestione.Camion[0], _gestione.TipiDiRifiuto[0]));
            _gestione.AggiungiTurno(new TurnoGiornaliero(new DateTime(1, 1, 1, 21, 30, 0), _inizio, _fine, 2, _gestione.Percorsi[0], _gestione.Persone, _gestione.Camion[0], _gestione.TipiDiRifiuto[0]));
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void AggiungiTurnoSovrappostoNelGiornoDiFine()
        {
            _gestione.AggiungiTurno(new TurnoGiornaliero(_ventitre, _inizio, _fine, 1, _gestione.Percorsi[0], _gestione.Persone, _gestione.Camion[0], _gestione.TipiDiRifiuto[0]));
            _gestione.AggiungiTurno(new TurnoGiornaliero(new DateTime(1, 1, 1, 21, 30, 0), _fine, _fine + new TimeSpan(24,0,0), 2, _gestione.Percorsi[0], _gestione.Persone, _gestione.Camion[0], _gestione.TipiDiRifiuto[0]));
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void AggiungiPercorsoDuplicato()
        {
            _gestione.AggiungiPercorso(new Percorso(new TimeSpan(2, 0, 0), 123, _gestione.Destinazioni[0], _gestione.PuntiDiRaccolta));
            _gestione.AggiungiPercorso(new Percorso(new TimeSpan(2, 0, 0), 123, _gestione.Destinazioni[0], _gestione.PuntiDiRaccolta));
        }
    }
}
