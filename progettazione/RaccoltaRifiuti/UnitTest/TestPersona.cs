﻿using NUnit.Framework;
using System;
using RaccoltaRifiuti.Model;

namespace UnitTest
{
    [TestFixture]
    public class TestPersona
    {
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void NomeNullo()
        {
            new Persona(null, "Rossi", "ABCDE12345FGHJKL");
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void CognomeNullo()
        {
            new Persona("Mario", null, "ABCDE12345FGHJKL");
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void CodiceFiscaleNullo()
        {
            new Persona("Mario", "Rossi", null);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void NomeVuoto()
        {
            new Persona(" ", "Rossi", "ABCDE12345FGHJKL");
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void CognomeVuoto()
        {
            new Persona("Mario", " ", "ABCDE12345FGHJKL");
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void CodiceFiscaleVuoto()
        {
            new Persona("Mario", "Rossi", " ");
        }

        [Test]
        public void DirittiAmministratore()
        {
            Persona p = new Persona("pippo", "pluto", "paperino", true);
            Assert.AreEqual(true, p.IsAmministratore);
            p.IsAmministratore = false;
            Assert.AreEqual(false, p.IsAmministratore);
        }
    }
}

