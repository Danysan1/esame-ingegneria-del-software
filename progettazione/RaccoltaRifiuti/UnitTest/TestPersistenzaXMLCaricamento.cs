﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RaccoltaRifiuti.Persistenza.XML;
using RaccoltaRifiuti.Model;
using NUnit.Framework;
using System.IO;
using RaccoltaRifiuti.Persistenza;

namespace UnitTest
{
    [TestFixture]
    class TestPersistenzaXMLCaricamento // Test su PersistenzaXML che usano "../../TestPersistenzaXMLCaricamento.xml"
    {
        private IPersistenza _persistenza;
        private List<TipoRifiuto> _esempioListaTipoDiRifiuto;
        private List<Destinazione> _esempioListaDestinazione;
        private List<PuntoDiRaccolta> _esempioListaPuntoDiRaccolta;
        private List<Persona> _esempioListaPersona;
        public List<Camion> _esempioListaCamion;
        public List<Percorso> _esempioListaPercorso;

        [SetUp]
        public void Setup()
        {
            _persistenza = new PersistenzaXML("../../TestPersistenzaXMLCaricamento.xml", true);

            _esempioListaDestinazione = new List<Destinazione>();
            _esempioListaDestinazione.Add(new Destinazione("pippo", "pluto"));

            _esempioListaPuntoDiRaccolta = new List<PuntoDiRaccolta>();
            _esempioListaPuntoDiRaccolta.Add(new PuntoDiRaccolta("pippo", "pluto"));

            _esempioListaTipoDiRifiuto = new List<TipoRifiuto>();
            _esempioListaTipoDiRifiuto.Add(new TipoRifiuto("umido"));

            _esempioListaPersona = new List<Persona>();
            _esempioListaPersona.Add(new Persona("pippo", "pluto", "paperino", true));

            _esempioListaCamion = new List<Camion>();
            _esempioListaCamion.Add(new Camion("ab123cd", _esempioListaTipoDiRifiuto));

            _esempioListaPercorso = new List<Percorso>();
            _esempioListaPercorso.Add(new Percorso(new TimeSpan(1, 0, 0), 12, new Destinazione("pippo", "pluto"), _esempioListaPuntoDiRaccolta));
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void FileNullo()
        {
            new PersistenzaXML(null, true);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void FileInvalido()
        {
            new PersistenzaXML(" ", true);
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void FileNonEsistente()
        {
            new PersistenzaXML("nonEsistente.xml", true);
        }

        private static readonly string nl = "nonLeggibile.xyz";
        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void FileSolaLettura()
        {
            if (!File.Exists(nl))
                File.Create(nl);
            File.SetAttributes(nl, FileAttributes.ReadOnly);
            new PersistenzaXML(nl, true);
        }

        [Test]
        public void CaricaCamion()
        {
            IEnumerable<Camion> c = _persistenza.CaricaCamion(_esempioListaTipoDiRifiuto);
            Assert.AreEqual(1, c.Count(), "camion");
            Assert.AreEqual(1, c.ElementAt(0).TipiSupportati.Count(), "tipi supportati camion");
            Assert.IsTrue(_esempioListaTipoDiRifiuto.Contains(c.ElementAt(0).TipiSupportati.ElementAt(0)), "tipo supportato valido");
        }

        [Test]
        public void CaricaDestinazioni()
        {
            Assert.AreEqual(1, _persistenza.CaricaDestinazioni().Count());
        }

        [Test]
        public void CaricaPersone()
        {
            Assert.AreEqual(2, _persistenza.CaricaPersone().Count());
        }

        [Test]
        public void CaricaPuntiDiRaccolta()
        {
            Assert.AreEqual(1, _persistenza.CaricaPuntiDiRaccolta().Count());
        }

        [Test]
        public void CaricaTipiDiRifiuto()
        {
            Assert.AreEqual(2, _persistenza.CaricaTipiDiRifiuto().Count());
        }

        [Test]
        public void CaricaTurni()
        {
            IEnumerable<Turno> t = _persistenza.CaricaTurni(_esempioListaPersona, _esempioListaTipoDiRifiuto, _esempioListaCamion, _esempioListaPercorso);
            Assert.AreEqual(1, t.Count(), "turni");
            Assert.AreEqual(1, t.ElementAt(0).Equipaggio.Count(), "equipaggio turno");
            Assert.IsTrue(_esempioListaPersona.Contains(t.ElementAt(0).Equipaggio.ElementAt(0)), "membro equipaggio valido");
        }

        [Test]
        public void CaricaPercorsi()
        {
            IEnumerable<Percorso> v = _persistenza.CaricaPercorsi(_esempioListaDestinazione, _esempioListaPuntoDiRaccolta);
            Assert.AreEqual(1, v.Count(), "Percorsi");
            Assert.AreEqual(1, v.ElementAt(0).Itinerario.Count(), "itinerario Percorso");
            Assert.IsTrue(_esempioListaPuntoDiRaccolta.Contains(v.ElementAt(0).Itinerario.ElementAt(0)), "punto itinerario valido");
        }

        [Test]
        public void CaricaCamionTurni()
        {
            IEnumerable<Camion> camion = _persistenza.CaricaCamion(_esempioListaTipoDiRifiuto);
            Assert.AreEqual(1, camion.Count(), "camion");
            Assert.AreEqual(1, camion.ElementAt(0).TipiSupportati.Count(), "tipi supportati camion");
            Assert.IsTrue(_esempioListaTipoDiRifiuto.Contains(camion.ElementAt(0).TipiSupportati.ElementAt(0)), "tipo supportato valido");

            IEnumerable<Turno> t = _persistenza.CaricaTurni(_esempioListaPersona, _esempioListaTipoDiRifiuto, camion, _esempioListaPercorso);
            Assert.AreEqual(1, t.Count(), "turni");
            Assert.AreEqual(1, t.ElementAt(0).Equipaggio.Count(), "equipaggio turno");
            Assert.IsTrue(_esempioListaPersona.Contains(t.ElementAt(0).Equipaggio.ElementAt(0)), "membro equipaggio valido");
        }

        [Test]
        public void CaricaTipiCamionTurni()
        {
            IEnumerable<TipoRifiuto> tipi = _persistenza.CaricaTipiDiRifiuto().ToList();
            Assert.AreEqual(2, tipi.Count(), "tipi");

            IEnumerable<Camion> camion = _persistenza.CaricaCamion(tipi);
            Assert.AreEqual(1, camion.Count(), "camion");
            Assert.AreEqual(1, camion.ElementAt(0).TipiSupportati.Count(), "tipi supportati camion");
            Assert.IsTrue(tipi.Contains(camion.ElementAt(0).TipiSupportati.ElementAt(0)), "tipo supportato valido");

            IEnumerable<Turno> t = _persistenza.CaricaTurni(_esempioListaPersona, tipi, camion, _esempioListaPercorso);
            Assert.AreEqual(1, t.Count(), "turni");
            Assert.AreEqual(1, t.ElementAt(0).Equipaggio.Count(), "equipaggio turno");
            Assert.IsTrue(_esempioListaPersona.Contains(t.ElementAt(0).Equipaggio.ElementAt(0)), "membro equipaggio valido");
        }

        [Test]
        public void CaricaTutto()
        {
            IEnumerable<Persona> persone = _persistenza.CaricaPersone().ToList();
            Assert.AreEqual(2, persone.Count(), "persone");

            IEnumerable<TipoRifiuto> tipi = _persistenza.CaricaTipiDiRifiuto().ToList();
            Assert.AreEqual(2, tipi.Count(), "tipi");

            IEnumerable<PuntoDiRaccolta> punti = _persistenza.CaricaPuntiDiRaccolta().ToList();
            Assert.AreEqual(1, punti.Count(), "punti");

            IEnumerable<Destinazione> destinazioni = _persistenza.CaricaDestinazioni();
            Assert.AreEqual(1, destinazioni.Count(), "destinazioni");

            IEnumerable<Camion> camion = _persistenza.CaricaCamion(tipi);
            Assert.AreEqual(1, camion.Count(), "camion");
            Assert.AreEqual(1, camion.ElementAt(0).TipiSupportati.Count(), "tipi supportati camion");
            Assert.IsTrue(tipi.Contains(camion.ElementAt(0).TipiSupportati.ElementAt(0)), "tipo supportato valido");

            IEnumerable<Percorso> Percorsi = _persistenza.CaricaPercorsi(destinazioni, punti);
            Assert.AreEqual(1, Percorsi.Count(), "Percorsi");
            Assert.AreEqual(1, Percorsi.ElementAt(0).Itinerario.Count(), "itinerario Percorso");
            Assert.IsTrue(punti.Contains(Percorsi.ElementAt(0).Itinerario.ElementAt(0)), "punto itinerario valido");

            IEnumerable<Turno> turni = _persistenza.CaricaTurni(persone, tipi, camion, Percorsi);
            Assert.AreEqual(1, turni.Count(), "turni");
            Assert.AreEqual(1, turni.ElementAt(0).Equipaggio.Count(), "equipaggio turno");
            Assert.IsTrue(persone.Contains(turni.ElementAt(0).Equipaggio.ElementAt(0)), "membro equipaggio valido");
        }
    }
}
