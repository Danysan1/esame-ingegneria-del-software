﻿# Casi d'uso

## Autenticazione
#### Attori
- Sconosciuto

#### Precondizioni
Nessuna

#### Postcondizioni
- L'attore diventa un utente e viene riconosciuto come amministratore o operatore

#### Scenario principale
1. L’attore inserisce nome utente e password
2. Nome e password sono corrette, l'attore viene riconosciuto

#### Scenari alternativi
- Codice fiscale non esistente fra gli operatori disponibili => Attore respinto
- Nessun amministratore ha impostato la password per il codice fiscale inserito => Attore respinto
- Password non corrisponde a quella valida per il codice fiscale inserito  => Attore respinto


---

## Disconnessione
#### Attori
 - Utente

#### Precondizioni
 - L'utente deve avere effettuato l'autenticazione

#### Postcondizioni
 - Il sistema non riconosce nessun utente autenticato

#### Scenario principale
 - L'utente effettua il logout
 - Il sistema non riconosce nessun utente autenticato

#### Scenari alternativi
Nessuno

---

## Visualizza turno
#### Attori
- Utente

#### Precondizioni
Nessuna

#### Postcondizioni
 - L'utente deve aver visualizzato il turno specificato

#### Scenario principale
1. L'utente specifica il turno da visulizzare
2. Il  turno specificato viene visualizzato

#### Scenari alternativi
- Il turno indicato non esiste => Messaggio di errore

---

## Crea turno
#### Attori
 - Amministratore

#### Precondizioni
- Almeno un Percorso è disponibile
- Almeno una Persona è disponibile
- Almeno un TipoRifiuto è disponibile
- Almeno un Camion è disponibile

#### Postcondizioni
 - Il turno specificato dall'utente viene creato

#### Scenario principale
1. L'attore inserisce i dati del nuovo turno (percorso, equipaggio, tipo di rifiuto e camion)
2. L'attore indica la volontà di aggiungere il turno specificato
3. Il turno specificato viene aggiunto

#### Scenari alternativi
 - Il turno indicato si sovrappone a un'altro turno pre-esistente => Messaggio di errore
 - Il tipo di rifiuto specificato non è supportato dal camion specificato => Messaggio di errore
 - Un valore specifico del tipo di turno non è valido (esempio per TurnoGiornaliero: data di fine precede data di inizio) => Messaggio di errore

---

## Elimina turno
#### Attori
 - Amministratore

#### Precondizioni
- Almeno un turno è presente

#### Postcondizioni
- Il  turno indicato dall'attore viene rimosso

#### Scenario principale
1. Viene richiesto all'attore quale turno eliminare
2. Il  turno indicato viene rimosso

#### Scenari alternativi
- Il turno indicato non esiste => Messaggio di errore

---

## Crea destinazione
#### Attori
 - Amministratore

#### Precondizioni
Nessuna

#### Postcondizioni
 - La destinazione specificata dall'utente viene creata

#### Scenario principale
1. Viene chiesta all'attore la via della nuova destinazione
2. La destinazione specificata viene aggiunta

#### Scenari alternativi
- La destinazione specificata esiste già (indirizzo già presente) => Messaggio di errore

---

## Elimina destinazione
#### Attori
- Amministratore

#### Precondizioni
- Almeno una destinazione è presente

#### Postcondizioni
- Una destinazione indicata dall'attore viene rimossa

#### Scenario principale
1. Viene richiesto all'attore quale destinazione eliminare (identificata dall'indirizzo)
2. La destinazione indicata viene rimossa

#### Scenari alternativi
- La destinazione indicata non esiste (indirizzo non presente) => Messaggio di errore

---

## Crea punto di raccolta
#### Attori
- Amministratore

#### Precondizioni
Nessuna

#### Postcondizioni
- Un punto di raccolta specificato dall'attore viene creato

#### Scenario principale
1. Vengono chiesti all'attore nome e via del nuovo punto di raccolta
2. Il punto di raccolta specificato viene aggiunto

#### Scenari alternativi
- Il punto di raccolta specificato esiste già (nome già presente) => Messaggio di errore

---

## Elimina punto di raccolta
#### Attori
- Amministratore

#### Precondizioni
- Almeno un punto di raccolta è presente

#### Postcondizioni
- Un punto di raccolta indicato dall'attore viene rimosso

#### Scenario principale
1. Viene richiesto all'attore quale punto di raccolta eliminare (identificato dal nome)
2. Il punto di raccolta indicato viene rimosso

#### Scenari alternativi
- Il punto di raccolta indicato non esiste (nome non presente) => Messaggio di errore

---

## Crea camion
#### Attori
- Amministratore

#### Precondizioni
Nessuna

#### Postcondizioni
- Un camion specificato dall'attore viene creato

#### Scenario principale
1. Vengono chiesti all'attore targa e tipi di rifiuto trasportabili
2. Il camion specificato viene aggiunto

#### Scenari alternativi
- Il camion specificato esiste già (targa già presente) => Messaggio di errore

---

## Elimina camion
#### Attori
- Amministratore

#### Precondizioni
- Presenza di almeno un camion 

#### Postcondizioni
- Il camion con la targa specificata viene eliminato

#### Scenario principale

1. L'amministratore inserisce la targa del camion da eliminare
2. Se la targa inserita esiste, viene eliminato il camion

#### Scenari alternativi
-  targa non esiste: si annulla l’operazione

---

## Crea tipo di rifiuto
#### Attori
- Amministratore

#### Precondizioni
Nessuna

#### Postcondizioni

- Aggiunta di un nuovo tipo di rifiuto

#### Scenario principale

1. L’amministratore inserisce il nome del nuovo tipo di rifiuto
2. Se il tipo non esiste prima, viene aggiunto il nuovo tipo

#### Scenari alternativi
-  Tipo esiste gia: si annulla l’operazione

---

## Elimina tipo di rifiuto
#### Attori
- Amministratore

#### Precondizioni
- Presenza del tipo di rifiuto da eliminare

#### Postcondizioni

- Il tipo di rifiuto specificato viene eliminato

#### Scenario principale

1. L’amministratore inserisce il nome del tipo di rifiuto da eliminare
2. Trovato il nome, il tipo di rifiuto viene eliminato

#### Scenari alternativi
-  Nome non trovato: si annulla l’operazione

---

## Crea utente
#### Attori
- Amministratore

#### Precondizioni
Nessuna

#### Postcondizioni

- Aggiunto un nuovo utente

#### Scenario principale

1. L’amministratore inserisce i dati del nuovo utente
2. Se il codice fiscale non esiste prima, l'utente viene inserito

#### Scenari alternativi
-  Codice fiscale esiste prima: si annulla l’operazione

---

## Elimina utente
#### Attori
- Amministratore

#### Precondizioni
- Presenza di almeno un utente

#### Postcondizioni
- L’utente specificato viene eliminato

#### Scenario principale

1. L’amministratore inserisce (codice fiscale) dell’utente da eliminare
2. Trovato l’utente con il (codice) specificato, l'utente viene eliminato

#### Scenari alternativi
-  (Codice) non esistente: si annulla l’operazione
