# Raccolta rifiuti

### Requisiti funzionali

Programma per la gestione degli orari del personale di una azienda per la raccolta dei rifiuti, che permetta di gestire orari, camion, itinerari, equipaggi e tipi di rifiuto.

Il programma pu� distinguere il tipo di rifiuto raccolto (ad es. plastica, vetro, indifferenziato), identificato con un nome univoco.

Ogni camion � caratterizzato dai tipi di rifiuto che pu� raccogliere e dalla targa univoca.

Ogni punto di raccolta � caratterizzato dal nome univoco e dalla via in cui si trova.

Ogni destinazione in cui un camion pu� sversare i rifiuti che ha raccolto (per esempio discarica, inceneritore, ...) � caratterizzata da un nome e un indirizzo univoco.

Ogni percorso � caratterizzato da una destinazione, una durata prevista e un itinerario formato da almeno un punto di raccolta.

Ogni persona � caratterizzata da nome, cognome e codice fiscale univoco. Un utente pu� avere autorizzazioni da operatore o da amministratore. 

Gli orari di lavoro sono organizzati in turni, ovvero insiemi di viaggi caratterizzati dallo stesso percorso, tipo di rifiuto, camion e equipaggio (gli operatori che gestiranno la raccolta). Il tipo di rifiuto raccolto in un turno lavorativo deve essere fra i tipi di rifiuto supportati dal camion utilizzato. Due turni aventi in comune lo stesso camion o almeno un operatore non si possono sovrapporre nel tempo, ovvero l�inizio di un viaggio di un turno non deve avvenire prima del termine previsto di un viaggio di un altro turno (ora di inizio + durata prevista) iniziato prima di lui. 

I turni sono attualmente organizzati in turni una tantum (composti da un solo viaggio), turni giornalieri (i cui viaggi avvengono tutti i giorni allo stesso orario in un certo intervallo di date) e turni settimanali (i cui viaggi avvengono una volta alla settimanain un certo intervallo di date, sempre nello stesso giorno della settimana e alla stessa ora). Non si esclude la necessit� in futuro di supportare nuovi tipi di turno.

All'inizio di ogni sessione l'utente dovr� effettuare l'autenticazione identificandosi con il codice fiscale e inserendo la propria password (impostata da un amministratore) e verr� identificato come amministratore o operatore. Gli utenti connessi devono potere visualizzare i dettagli dei viaggi previsti di cui faranno parte. Gli utenti amministratori devono inoltre poter visualizzare, aggiungere e eliminare tutti i dati (camion, destinazioni, percorsi, persone, tipi di rifiuto, punti di raccolta, turni). Un amministratore pu� promuovere un utente a amministratore o demansionare un amministratore (incluso se stesso, a condizione che non sia l�unico) a operatore.

### Requisiti non funzionali

Utilizzo del framework .NET e del linguaggio C#.

### Possibili estensioni

Il programma deve tenere conto del fatto che il camion, gli utenti e i punti di raccolta siano ancora presenti e funzionanti o meno,
impedendo l'aggiunta di turni che li coinvolgano se non sono pi� presenti.
