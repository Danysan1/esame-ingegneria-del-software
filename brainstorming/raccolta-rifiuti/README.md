# 📝 Proposta 📝

# Raccolta rifiuti

Programma gestionale per la gestione della raccolta di rifiuti, che permetta di gestire gli orari e gli itinerari dei camion per la raccolta.

Ogni viaggio di raccolta è caratterizzato da un camion, un tipo di rifiuto, una data/ora, almeno un punto di raccolta.

Un camion è caratterizzato univocamente dalla targa.

Il programma può distinguere il tipo di rifiuto raccolto (ad es. differenziato, indifferenziato). In ogni viaggio si può raccogliere un solo tipo di rifiuto.

Il programma gestisce il personale che si occupa della raccolta e la destinazione (ad es. discarica).


## Possibili classi nel mondo reale
- Viaggio di raccolta
- Punto di Raccolta
- Tipo di rifiuto
  - Differenziato
  - Indifferenziato
- Utente
  - Operatore
  - Amministratore
- Camion
  - compattatore
- Destinazione


## Altre possibilità
Gestiire l'orario di lavoro