# 📝 Proposta 📝
# ⚠ Scartata ⚠

# Azienda agricola
Realizzare un sistema per la gestione delle scorte del magazzino di una azienda agricola, permettendo di preventivare le spese future.

I prodotti in magazzino possono appartenere alle seguenti categorie:

- Sementi
- Prodotti fitosanitari (fungicidi, diserbanti, insetticidi, fitoregolatori, ...)
- Fertilizzanti

Ogni prodotto è caratterizzato da ID univoco, nome commerciale, prezzo unitario, quantità in magazzino (obbligatori) e note (opzionali).

Deve essere possibile creare la lista di elementi necessari per la gestione una singola pianta di una certa specie, indicando per ognuno la quantità.

Data la quantità di piante in una piantagione e fornita la lista per quella specie, il sistema deve indicare quali e quanti elementi sono necessari, quali sono già disponibili in magazzino e quali vanno acquistati, preventivando il costo dell'acquisto.

## Software
- .NET + CSharp (imposto)

