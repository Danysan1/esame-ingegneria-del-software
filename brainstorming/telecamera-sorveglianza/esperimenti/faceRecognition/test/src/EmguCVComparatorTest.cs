﻿using NUnit.Framework;
using System;
using System.Drawing;

namespace test
{
	[TestFixture ()]
	public class EmguCVImageComparatorTest
	{
		ImageComparator comparator;

		[SetUp ()]
		public void prepara ()
		{
			comparator = new EmguCVComparator ();
		}

		[Test ()]
		[ExpectedException (typeof(ArgumentNullException))]
		public void LeftNullCompare ()
		{
			comparator.compareImage (Image.FromFile("../../img/a.jpg"), null);
		}

		[Test ()]
		[ExpectedException (typeof(ArgumentNullException))]
		public void RightNullCompare ()
		{
			comparator.compareImage (null, Image.FromFile("../../img/a.jpg"));
		}

		[Test ()]
		[ExpectedException (typeof(ArgumentNullException))]
		public void BothNullCompare ()
		{
			comparator.compareImage (null, null);
		}

		[Test ()]
		public void UnknownCompare ()
		{
			Assert.AreEqual (false, comparator.compareImage (Image.FromFile("../../img/a.jpg"), Image.FromFile("../../img/a.jpg")));
		}
	}
}

