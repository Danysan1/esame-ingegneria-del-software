# Face detection
- http://docs.opencv.org/2.4/doc/tutorials/objdetect/cascade_classifier/cascade_classifier.html
- http://blogs.interknowlogy.com/2013/10/21/face-detection-for-net-using-emgucv/
- http://ahmedopeyemi.com/main/face-detection-and-recognition-in-c-using-emgucv-3-0-opencv-wrapper-part-1/

# Face recognition
- http://docs.opencv.org/2.4/modules/contrib/doc/facerec/facerec_api.html
- http://www.emgu.com/wiki/files/3.1.0/document/html/62795d93-e4b8-3c3a-f11f-42ce97895c0e.htm
- http://ahmedopeyemi.com/main/face-detection-and-recognition-in-c-using-emgucv-3-0-opencv-wrapper-part-2/
