﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private Capture _capture;
        private CascadeClassifier _cascadeClassifier;

        public Form1()
        {
            InitializeComponent();
            _capture = new Capture();
            _cascadeClassifier = new CascadeClassifier(Application.StartupPath + "..\\..\\..\\haarcascade_frontalface_alt_tree.xml");
        }

        private void Form1_Load(object sender, EventArgs e)
        {/*
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += new DoWorkEventHandler(delegate (object o, DoWorkEventArgs args)
           {
               while (true)
               {
                   imgCamUser.Image = _capture.QueryFrame();
                   System.Threading.Thread.Sleep(1000);
               }
           });*/
        }

        private void imgCamUser_Click(object sender, EventArgs e)
        {
            //imgCamUser.Image = _capture.QueryFrame();
            
            using (var imageFrame = _capture.QueryFrame().ToImage<Bgr, Byte>())
            {
                if (imageFrame != null)
                {
                    var grayframe = imageFrame.Convert<Gray, byte>();
                    var faces = _cascadeClassifier.DetectMultiScale(grayframe, 1.1, 10, Size.Empty); //the actual face detection happens here
                    foreach (var face in faces)
                    {
                        imageFrame.Draw(face, new Bgr(Color.BurlyWood), 3); //the detected face(s) is highlighted here using a box that is drawn around it/them

                    }
                }
                imgCamUser.Image = imageFrame;
            }
        }
    }
}