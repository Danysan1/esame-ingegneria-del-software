﻿using System;
using System.Speech.Synthesis;

namespace tts
{
    class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("Hello World!");

            SpeechSynthesizer synth = new SpeechSynthesizer();
            synth.Speak("Hello World!");
            synth.Speak("Ciao mondo!");
            synth.Speak("Bye bye!");
        }
	}
}
