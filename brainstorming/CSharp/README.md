# 📖 Guida 📖

# C Sharp

## Guide
- [Wikibooks](https://en.wikibooks.org/wiki/C_Sharp_Programming)

## Libri
- [C# 5.0 in a nutshell](http://devevolution.com/wp-content/uploads/2012/12/Oreilly.Csharp.5.0.in_.a.Nutshell.5th.Edition.June_.2012.pdf)
