# Presentazione progetto
[Modalità](http://lia.disi.unibo.it/Courses/IngSwT1617/ModalitaDiEsame.pdf)

1. Daniele
   1. Descrizione del problema. 
   2. Documento dei requisiti. 
   3. Glossario (solo nella relazione). 
   4. Diagramma dei casi d'uso. 
   5. Descrizione dettagliata di 1+ casi d’uso (significativi). 
2. Fady
   1. Prototipo. 
   2. Diagramma delle classi di analisi con 10-15 classi di cui almeno 7 significative (non in gerarchia) 
   3. Un diagramma di sequenza o un diagramma di stato. 
3. Andrea
   1. Diagramma delle classi modificato in seguito alle scelte progettuali. 
   2. Discussione sui design pattern (1+, singleton escluso) e sui principi di progettazione utilizzati. 